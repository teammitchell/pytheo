# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging


from django.test import TestCase

from djpytheo.models import DjangoTheoData, DjangoTheo, DjangoSingular, DjangoTheoSlot, DjangoMultiple
from pytheo import NO_THEO_VALUE, Theo, Multiple
from pytheo import kb


class NativepytheoTest(TestCase):
	def setUp(self):
		self.theo = Theo()
		self.theo.load_kb()
	def _city_mayor_kb(self):
		city = self.theo.Entity("city")
		pgh = self.theo.Entity("Pittsburgh",city)
		tor = self.theo.Entity("Toronto",city)
		bal = self.theo.Entity("Baltimore",city)
		person = self.theo.Entity("person")
		peduto = self.theo.Entity("Bill Peduto",person)
		masloff = self.theo.Entity("Sophie Masloff",person)
		rowlands = self.theo.Entity("June Rowlands",person)
		tory = self.theo.Entity("John Tory",person)
		young = self.theo.Entity("Jack Young",person)
		dixon = self.theo.Entity("Sheila Dixon",person)
		mayor = self.theo.Slot("mayor", slots=[("domain",city)], range=person)
		term = self.theo.Slot("term", domain=mayor)
		return pgh,peduto,masloff,tor,mayor,term,bal,dixon,young
	def test_typecheck(self):
		kb.classic(self.theo)
		pgh,peduto,masloff,tor,mayor,term,bal,dixon,young = self._city_mayor_kb()

		logging.getLogger('pytheo').setLevel(logging.WARNING)

		self.assertTrue(self.theo.typecheck_domain("mayor",pgh))
		self.assertFalse(self.theo.typecheck_domain("mayor",peduto))
		self.assertTrue(self.theo.typecheck_range("mayor",peduto))
		self.assertFalse(self.theo.typecheck_range("mayor",pgh))

		pgh_peduto = pgh.slot_set("mayor",peduto)[-1]
		self.assertTrue(self.theo.typecheck_domain("term",pgh_peduto))
		self.assertFalse(self.theo.typecheck_domain("term",pgh))
		self.assertFalse(self.theo.typecheck_domain("term",peduto))
		self.assertTrue(self.theo.typecheck_range("term","2014-present"))
		self.assertTrue(self.theo.typecheck_range("term",pgh))
		self.assertTrue(self.theo.typecheck_range("term",peduto))
		self.assertTrue(self.theo.typecheck_range("term",pgh_peduto))
	def test_slotslot(self):
		pgh,peduto,masloff,tor,mayor,term,bal,dixon,young = self._city_mayor_kb()
		logging.getLogger('pytheo').setLevel(logging.WARNING)
		ll = logging.getLogger('pytheo.tests')

		pgh_peduto = pgh.slot_set("mayor",peduto)[-1]
		pgh_peduto.slot_set("term","2014-present")
		pgh_masloff = pgh.slot_set("mayor",masloff)[-1]
		pgh_masloff.slot_set("term","1988-1994")

		tor_tory = tor.slot_set(mayor,"John Tory")[-1]
		tor_tory.slot_set(term,"2014-present")
		tor_rowlands = tor.slot_set(mayor,"June Rowlands")[-1]
		tor_rowlands.slot_set(term,"1991-1994")


		bal_dixon = bal.slot_set(mayor,dixon)[-1]
		bal_dixon_q = self.theo.fetch("Baltimore","mayor")
		self.assertIsInstance(bal_dixon_q,Multiple)
		ll.debug(bal_dixon_q)
		self.assertEqual(bal_dixon,bal_dixon_q[0])
		bal_dixon_q[0].slot_set(term,"2007-2010")
		bal_young = bal.slot_set(mayor,young)[-1]
		bal_young_q = self.theo.fetch("Baltimore.mayor")
		self.assertIsInstance(bal_young_q,Multiple)
		ll.debug(bal_young_q)
		self.assertEqual(bal_young,bal_young_q[1])
		bal_young_q[1].slot_set(term,"2019-present")

		for x in [pgh_peduto,pgh_masloff,tor_tory,tor_rowlands,bal_dixon,bal_young]:
			t  = x.slot_get("term")
			self.assertIsInstance(t,Multiple)
			self.assertEqual(1,len(t),t)
	def test_delete(self):
		ll = logging.getLogger("djpytheo.tests")
		ll.setLevel(logging.DEBUG)
		# logging.getLogger("pytheo.theo").setLevel(logging.DEBUG)

		# define terms
		mr_rogers = self.theo.Entity("Mister Rogers")
		joe_negri = self.theo.Entity("Joe Negri")
		betty_aberlin = self.theo.Entity("Betty Aberlin")
		friend = self.theo.Slot("friend")

		# add facts
		mr_rogers.slot_set("friend","chef brockett")
		mr_rogers.slot_set(friend,"maggie stewart")
		mr_rogers.slot_set("friend",joe_negri)
		mr_rogers.slot_set(friend,betty_aberlin)
		
		mr_rogers_friend = mr_rogers.slot_get("friend")
		self.assertNotEqual(mr_rogers_friend,NO_THEO_VALUE)
		self.assertEqual(len(mr_rogers_friend),4,"initial setup")
		self.assertEqual(mr_rogers_friend[0].value,"chef brockett")
		self.assertEqual(mr_rogers_friend[1].value,"maggie stewart")
		self.assertEqual(mr_rogers_friend[2].value,joe_negri)
		self.assertEqual(mr_rogers_friend[3].value,betty_aberlin)

		mr_rogers.slot_delete(friend,"chef brockett")
		mr_rogers_friend = mr_rogers.slot_get("friend")
		ll.debug(mr_rogers_friend)
		self.assertNotEqual(mr_rogers_friend,NO_THEO_VALUE)
		self.assertEqual(len(mr_rogers_friend),3,"delete chef brockett")
		self.assertEqual(mr_rogers_friend[0].value,"maggie stewart")
		self.assertEqual(mr_rogers_friend[1].value,joe_negri)
		self.assertEqual(mr_rogers_friend[2].value,betty_aberlin)

		mr_rogers.slot_delete(friend,joe_negri)
		mr_rogers_friend = mr_rogers.slot_get("friend")
		ll.debug(mr_rogers_friend)
		self.assertNotEqual(mr_rogers_friend,NO_THEO_VALUE)
		self.assertEqual(len(mr_rogers_friend),2,"delete joe negri")
		self.assertEqual(mr_rogers_friend[0].value,"maggie stewart")
		self.assertEqual(mr_rogers_friend[1].value,betty_aberlin)

		mr_rogers.slot_delete("friend","maggie stewart")
		mr_rogers_friend = mr_rogers.slot_get("friend")
		ll.debug(mr_rogers_friend)
		self.assertNotEqual(mr_rogers_friend,NO_THEO_VALUE)
		self.assertEqual(len(mr_rogers_friend),1,"delete maggie stewart")
		self.assertEqual(mr_rogers_friend[0].value,betty_aberlin)

		mr_rogers.slot_delete("friend",betty_aberlin)
		mr_rogers_friend = mr_rogers.slot_get("friend")
		ll.debug(mr_rogers_friend)
		self.assertEqual(mr_rogers_friend,NO_THEO_VALUE,"delete betty aberlin")

class DjpytheoTest(NativepytheoTest):
	def setUp(self):
		# logging.getLogger('pytheo').setLevel(logging.DEBUG)
		logging.getLogger('djpytheo').setLevel(logging.WARNING)

		self.theo = DjangoTheo()
		self.theo.c.impl.Data = DjangoTheoData
		self.theo.c.impl.Slot = DjangoTheoSlot
		# theo.c.logging.trace = True
		self.theo.load_kb()
	def tearDown(self):
		self.theo.unregister()
	def test_startup_kb(self):
		ll = logging.getLogger("djpytheo.tests")
		for x in "everything slot specializations generalizations".split():
			self.assertIn(x,self.theo.KB, "'%s' missing"%x)
			ent = self.theo.KB[x]
			self.assertIsInstance(ent,DjangoMultiple, "Bad type KB[%s]: %s %r" % (x,type(ent),ent))
			ll.info(ent)
			ll.info(ent[0])
			val = ent[0].value
			# val should be NO_THEO_VALUE or DjangoMultiple or str
			# val should not be the string value 'TheoConstant(NO_THEO_VALUE)'
			self.assertEqual(val,NO_THEO_VALUE,"Bad value for KB[%s]: %s %r"%(x,type(val),val))
	def _city_kb(self):
		city = self.theo.Entity("city")
		pgh = self.theo.Entity("Pittsburgh",city)
		tor = self.theo.Entity("Toronto",city)
		bal = self.theo.Entity("Baltimore",city)
		self.theo.Slot("plays",slots=[("domain",city)], range=city)
		logging.getLogger("pytheo.theo.slot_set").setLevel(logging.WARNING)
		pgh.slot_set("plays",tor)
		bal.slot_set("plays",pgh)
		bal.slot_set("plays",tor)
		return (city,pgh,tor,bal)
	def test_load_kb(self):
		ll = logging.getLogger("djpytheo.tests")
		ll.info("================ starting test test_load_kb =======================")
		(city,pgh,tor,bal) = self._city_kb()

		cases = [
			( ("Pittsburgh","plays"), {tor} ),
			( ("plays","domain"), {city} ),
			( ("Baltimore","plays"), {pgh,tor} )
		]

		for q,a in cases:
			try:
				self.assertIn(".".join(q), self.theo.KB, f"No {q} in KB")
				for v in self.theo.fetch(*q).value:
					self.assertNotEqual(NO_THEO_VALUE,v,f"Unexpected NO_THEO_VALUE in {q}")
					self.assertIn(v, a, "%s !in %s" % (v,a))
			except TypeError:
				ll.error("on (q,a)=(%r,%r)",q,a)
				raise

		tor_opponents = self.theo.fetch_by_value("plays",tor)
		ll.info(tor_opponents)
		self.assertEqual(2,len(tor_opponents))

		city_relations = self.theo.fetch_by_value("range",city)
		ll.info(city_relations)
		self.assertEqual(1,len(city_relations))
	def _check_classic_kb(self):
		ll = logging.getLogger("djpytheo.tests")

		slot = self.theo.fetch("slot")[0]
		slot_relations = self.theo.fetch_by_value("domain",slot)
		self.assertGreater(len(slot_relations),0)

		theoslotslot = self.theo.fetch("theoSlotSlot")[0]
		anc = self.theo.ancestors(theoslotslot)
		self.assertEqual(3,len(anc), f"wrong # of ancestors on theoSlotSlot: {anc}")

		toy = self.theo.c.impl.Singular(self.theo, "toySlot", NO_THEO_VALUE, generalizations=theoslotslot)
		ll.debug(toy)
		self.assertFalse(toy.has_slot("domain"), "shouldn't be in there yet")
		toy_domain = toy.slot_get("domain")
		ll.debug(toy_domain)
		self.assertEqual(slot, toy_domain[0].value)
		self.assertTrue(toy.has_slot("domain"),"Didn't cache properly")
	def test_classic(self):
		logging.getLogger("djpytheo").setLevel(logging.WARNING)
		kb.classic(self.theo)
		self._check_classic_kb()
	def test_classic_batch(self):
		self.theo.start_batch()
		kb.classic(self.theo)
		self.theo.finish_batch()
		self._check_classic_kb()
	def test_django_deletions(self):
		ll = logging.getLogger("djpytheo.tests")
		(city,pgh,tor,bal) = self._city_kb()
		self.assertIn("Pittsburgh", self.theo.KB)
		cities = city.slot_get(self.theo.c.preds.subtype)
		self.assertEquals(3,len(cities))

		plays_slots = self.theo.c.impl.Slot.objects.filter(slot="plays").values('head__name','slot','tail__name')
		for s in plays_slots:
			ll.debug(s)
		ll.debug("deleting Pittsburgh to check plays")
		pgh.data.delete()
		self.assertNotIn("Pittsburgh", self.theo.KB)
		plays_slots = self.theo.c.impl.Slot.objects.filter(slot="plays").values('head__name','slot','tail__name')
		for s in plays_slots:
			ll.debug(s)
		self.assertNotIn("Pittsburgh.plays",self.theo.KB)
		ll.debug("instantiating Pittsburgh to check that plays is still absent")
		pgh = self.theo.Entity("Pittsburgh",city)
		self.assertFalse(pgh.has_slot("plays"))
		ll.debug("deleting Pittsburgh to check city.specializations")
		pgh.delete()
		self.assertNotIn("Pittsburgh", self.theo.KB)
		cities = city.slot_get(self.theo.c.preds.subtype)
		self.assertEquals(2,len(cities))



