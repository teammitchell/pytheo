import traceback
from collections import defaultdict
from itertools import chain

import django.db.models as mdl
from django.db import connection, IntegrityError
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models.signals import pre_delete, post_delete
from django.dispatch import receiver
from pytheo.theo import Singular, Multiple, Theo, TheoItem, TheoConstant, NO_THEO_VALUE, TheoCache, stackTrace
import logging
def logger(suffix=None,override=None):
	if override is None:
		name = __name__
		if suffix: name += "." + suffix
		return logging.getLogger(name)
	return logging.getLogger(override)

class EmptyValue(Exception):
	def __init__(self,data,msg):
		self.data=data
		super(Exception,self).__init__(msg)

class DjangoTheoCache(TheoCache):
	def uncache(self, key, pk, **kwargs):
		return super(DjangoTheoCache,self).uncache(
			key,
			pk,
			viz=lambda x:x.data.pk,
			eq=lambda x:x.data.pk==pk)

class DjangoKB(DjangoTheoCache):
	def __init__(self, theo):
		super(DjangoKB,self).__init__(theo, "kb")
		# [logger(self.logkey).debug("stack\n%s",x[:-1]) for x in traceback.format_stack(limit=8)]
	def _init_cache(self):
		super(DjangoKB,self)._init_cache()
		self.django_cache = {}
		self._names = set()
		entities = []
		singulars = {}

		# debugging cache invalidation bugs is wretched, so just load the whole gd database

		for data in self.theo.c.impl.Data.objects.all():
			self.django_cache[data.pk] = data
			singulars[data.pk] = self.theo.c.impl.Singular(self.theo,data,msg="deserialize")
			self._names.add(data.name)
			if data.name.find(".")<0: entities.append( (data.name, data.pk) )
		valuedata = defaultdict(list)
		for value in self.theo.c.impl.Value.objects.all():
			valuedata[value.data_id].append(value)
		for pk,lst in valuedata.items():
			self.dj_fetch(pk).init_value(values=lst,force=True)
		slotdata = defaultdict(list)
		deserialize = defaultdict(set)
		for head_pk,slot,name,tail_pk in self.theo.c.impl.Slot.objects.all().values_list('head_id','slot','tail__name','tail_id'):
			slotdata[ (name,singulars[head_pk]) ].append(tail_pk)
			deserialize[head_pk].add(slot)
		for pk,slots in deserialize.items():
			singulars[pk]._init_slots(slots)
		for name,pk in entities:
			self._cacheAdd(
				name,
				NO_THEO_VALUE,
				self.theo.c.impl.Multiple(self.theo,name,[singulars[pk]]),
				"NO_THEO_VALUE",
				logger(self.logkey))
		for (name,head),lst in slotdata.items():
			self._cacheAdd(
				name,
				head,
				self.theo.c.impl.Multiple(self.theo,name,[singulars[k] for k in lst],head=head),
				head.short_str(),
				logger(self.logkey))

	def _init_names(self):
		if not self.cache:
			logger(self.logkey).debug("initializing names...")
			self._names = set(self.theo.c.impl.Data.objects.only("name")
							  .values_list("name",flat=True)
							  .distinct())
			logger(self.logkey).debug("%d names found in DB",len(self._names))
		logger(self.logkey).debug(self._names)
		logger(self.logkey).debug("%s in KB: %s",self.theo.c.root.entity,self.theo.c.root.entity in self._names)
	def dj_add(self,data):
		if data.pk not in self.django_cache:
			self.django_cache[data.pk] = data
	def dj_remove(self,data=None,pk=None):
		if data:
			pk = data.pk
		if pk in self.django_cache: del self.django_cache[pk]
	def dj_fetch(self,pk): # may need to add a refresh_from_db here if we add bulk updates anywhere in future
		return self.django_cache[pk]
	def dj_contains(self,pk):
		return pk in self.django_cache
	def dj_convert(self,data):
		if not self.dj_contains(data.pk):
			self.dj_add(data)
		return self.dj_fetch(data.pk)
	def dj_assemble(self,lst):
		return [self.dj_convert(r) for r in lst]
	def dj_prefetch(self,head,tails_names):
		"""
		Fetch a bunch of slots on some entity at once
		:param head: the parent entity
		:param tails_names: the slots to fetch from the database
		:return:
		"""
		fetch_names = []
		cache_names = []
		for tn in tails_names:
			if (tn,head) in self.cache:
				cache_names.append(tn)
			else:
				fetch_names.append(tn)
		# fetch_names = [tn for tn in tails_names if (tn,head) not in self.cache]
		results = self.theo.c.impl.Slot.objects.filter(head=head.data,tail__name__in=fetch_names).select_related('tail')
		aggregate = defaultdict(list)
		results = self.dj_assemble([r.tail for r in results])
		logger(self.logkey).debug("dj_prefetch: %r",[x.pk for x in results])
		if results:
			self.theo._load_batch_values(results,recurse=1)
			results = self.theo._deserialize_batch(results)
		# cache the fetched results
		for r in results:
			aggregate[r.name].append(r)
		drop_deleted_from_lst(aggregate.values(),self.theo,logger("DjangoKB.status"),in_cache=False,header=list(aggregate.keys())) # was logger(self.logkey)
		ret = []
		for name in tails_names:
			if name not in aggregate:
				if (name,head) in self.cache:
					logger("DjangoKB.status").debug("dj_prefetch: filling %s from cache",name)
					ret.append(self[(name,head)])
				else:
					logger("DjangoKB.status").debug("dj_prefetch: filling %s as NO_THEO_VALUE query miss",name)
					ret.append(NO_THEO_VALUE)
				continue
			lst = aggregate[name]
			if lst:
				logger("DjangoKB.status").debug("dj_prefetch: filling %s as Multiple from query",name)
				m = self.theo.c.impl.Multiple(self.theo,lst[0].name,lst,head)
				self[(lst[0].name,head)] = m
				ret.append(m)
			else:
				logger("DjangoKB.status").debug("dj_prefetch: filling %s as NO_THEO_VALUE from query",name)
				ret.append(NO_THEO_VALUE)
		return ret
	def uncache(self, key, pk, **kwargs):
		self.dj_remove(pk=pk)
		return super(DjangoKB,self).uncache(key,pk,**kwargs)
	def __getitem__(self, key):
		ll = logger(f"{self.logkey}.get")
		ret = DjangoTheoCache.__getitem__(self, key)
		if isinstance(key,tuple):
			name,head = key
		elif key.find(".")<0:
			name,head = key,NO_THEO_VALUE
		else:
			name,head = key,None
		head_str = head.short_str() if head else head
		if ret != NO_THEO_VALUE:
			ll.debug("(%s,%s): in cache",name,head_str)
			return ret

		ll.debug("(%s,%s): from db",name,head_str)
		if head and head != NO_THEO_VALUE:
			intermediate = self.theo.c.impl.Slot.objects.filter(tail__name=name,head=head.data.pk).select_related('tail')
			results = [x.tail for x in intermediate]
		else:
			results = list(self.theo.c.impl.Data.objects.filter(name=name))
		if len(results) == 0:
			ll.debug("(%s,%s): nothing retrieved",name,head_str)
			return NO_THEO_VALUE
		results = self.dj_assemble(results)
		ret = DjangoMultiple(
			self.theo,
			name,
			self.theo._deserialize_batch(results)
		)
		if head and head != NO_THEO_VALUE:
			ret.head = head
		if ll.isEnabledFor(logging.DEBUG):
			ll.info("caching from db %s@%s = [%s]",name,head_str, ",".join(x.short_str() for x in ret))
		self._cacheAdd(name, head, ret, head_str, ll)# cache[(name,head)] = ret
		return ret
	def __setitem__(self, key, value):
		super(DjangoKB,self).__setitem__(key,value)
		if isinstance(value,Singular):
			value = [value]
		for v in value: self.dj_add(v.data)
	def __iter__(self):
		return self.theo.c.impl.Data.objects.all

class DjangoTheo(Theo):
	def __init__(self,config=None):
		Theo.__init__(self,config)
		# overwrite defaults
		self.c.storage="memory"
		self.c.impl.Singular = DjangoSingular
		self.c.impl.Multiple = DjangoMultiple
		self.c.impl.DjangoKB = DjangoKB
		self.c.impl.Data     = DjangoTheoData
		self.c.impl.Value    = DjangoTheoValue
		self.c.impl.Slot     = DjangoTheoSlot
		assert not hasattr(DjangoTheoData,'THEO'), "You can only have one DjangoTheo at a time -- what happened to the first one?"
		DjangoTheoData.THEO=self
	def _pre_load(self):
		self.KB = self.c.impl.DjangoKB(self)
	def load_kb(self):
		Theo.load_kb(self)
		r = self.c.impl.Data.objects.get(name=self.c.root.entity)
	def fetch(self, *address):
		try:
			ret = super(DjangoTheo,self).fetch(*address)
			if ret != NO_THEO_VALUE: drop_deleted_from_lst(ret,self,logger())
		except EmptyValue as e:
			if e.data.name == ".".join(address):
				e.data.delete()
				return NO_THEO_VALUE
		return ret

	def fetch_by_value(self, slot, val, ancestors=False):
		sql_cursor = len(connection.queries)
		llsql = logger('sql')
		llsql.debug('begin fetch_by_value %s',slot)

		q = {'slot':slot}
		# q = {'data__name':slot}
		group = []
		if ancestors:
			group = [val] + self.ancestors(val if isinstance(val,self.c.impl.Singular) else self.c.impl.Singular(self,val,msg='deserialize'))
		if isinstance(val,self.c.impl.Singular):
			val=val.data

		if isinstance(val,self.c.impl.Data):
			if ancestors:
				q['tail__values__value_f_id__in'] = [x.data.pk for x in group]
				# q['value_f__id__in'] = [x.data.pk for x in group]
			else:
				q['tail__values__value_f_id'] = val.pk
				# q['value_f__id'] = val.pk
		else:
			q['tail__values__value_c'] = val
			# q['value_c'] = val
		r = list(self.c.impl.Slot.objects.filter(**q).select_related('head'))
		# r = list(self.c.impl.Value.objects.filter(**q).values_list('data_id',flat=True))
		ll = logger('util')
		ll.debug("fetch by value %r:",q)
		if ll.isEnabledFor(logging.DEBUG): ll.debug("values %r",[x.pk for x in r])
		r = self.KB.dj_assemble(x.head for x in r)

		# if llsql.isEnabledFor(logging.DEBUG):
		# 	for q in connection.queries[sql_cursor:]:
		# 		llsql.debug(q)
		llsql.debug('end fetch_by_value %s',slot)

		return self._deserialize_batch(r)
		# return [self.c.impl.Singular(self,x.head,msg="deserialize") for x in ret]
	def fetch_by_orm(self, multiple=False, **query):
		try:
			if multiple:
				ret = self.c.impl.Data.objects.filter(**query)
			else:
				ret = self.c.impl.Data.objects.get(**query)
		except Exception as e:
			logger().info(e)
			return None
		else:
			if multiple:
				return [self.c.impl.Singular(self,self.KB.dj_convert(r),msg="deserialize") for r in ret]
			return self.c.impl.Singular(
				self,self.KB.dj_convert(ret),
				msg="deserialize")
	def start_batch(self):
		DjangoTheoSlot.BATCH=True
	def finish_batch(self):
		DjangoTheoSlot.finish_batch()
	def unregister(self):
		if hasattr(DjangoTheoData,"THEO"):
			del DjangoTheoData.THEO
	def transitiveClosure(self, entityref, slot, visited=None, exclusions=[], quarry=None):
		# for now we assume entityref is a TheoItem and we're only interested in TheoItem results
		logger(override="pytheo.theo.transitive.djpytheo").debug("Closure for %s on %r",slot,entityref)
		closure = self._transitiveClosure([{entityref.data.pk},{entityref.data.pk}],slot)
		ret = list()
		# rearrange things so the starting node is always first
		ret.append(entityref)
		closure.remove(entityref.data.pk) # don't duplicate the starting node
		ret.extend(self._deserialize_batch([self.KB.dj_fetch(r) for r in closure]))
		return ret
	def massTransitiveClosure(self, entities, slot):
		pks = [e.data.pk for e in entities]
		return self._deserialize_batch([
			self.KB.dj_fetch(r) for r in self._transitiveClosure([
				set(pks),set(pks)
			],slot)
		])
	def _transitiveClosure(self,closure_accumulator,slot):
		while closure_accumulator[-1]:
			query = self.c.impl.Slot.objects.filter(head__in=closure_accumulator[-1],slot=slot).values_list('tail__values__value_f',flat=True)
			gg = set(y for y in query if (y and y not in closure_accumulator[0]))
			closure_accumulator.append(gg)
			closure_accumulator[0].update(gg)

		retrieve = [r for r in closure_accumulator[0] if not self.KB.dj_contains(r)]
		results = self.c.impl.Data.objects.in_bulk(retrieve)
		for r in results.values(): self.KB.dj_add(r)
		return closure_accumulator[0]

	def _load_batch_values(self, data_lst=None, pk_lst=None, recurse=0):
		if data_lst is None:
			assert not pk_lst is None
			fetch = [pi for pi in pk_lst if not self.KB.dj_contains(pi)]
			for di in self.c.impl.Data.objects.filter(pk__in=fetch):
				self.KB.dj_add(di)
			data_lst = [self.KB.dj_fetch(pi) for pi in pk_lst]
		# fetch, aggregate, and pre-initialize value data #when batch size is >1
		uninitialized_values = [di for di in data_lst if not di._value_is_init]
		if not uninitialized_values: return []
		valuedata_rows = self.c.impl.Value.objects.filter(data_id__in=[di.pk for di in uninitialized_values])
		valuedata = defaultdict(list)
		value_f_pks = set()
		valuedata_by_value_f_pk = defaultdict(list)
		for value in valuedata_rows:
			valuedata[value.data_id].append(value)
			if recurse>0 and value.value_f_id != None:
				value_f_pks.add(value.value_f_id)
				valuedata_by_value_f_pk[value.value_f_id].append(value)
		if value_f_pks:
			for value_f in self._load_batch_values(pk_lst=value_f_pks,recurse=recurse-1):
				for vi in valuedata_by_value_f_pk[value_f.pk]:
					logger('_load_batch_values').debug("filling value_f on %s",vi.pk)
					vi.value_f = value_f # no clue if this works

		for di in uninitialized_values:
			if di.pk in valuedata:
				di.init_value(values=valuedata[di.pk],force=True) # is force really necessary here?
		return data_lst

	def _deserialize_batch(self, data_lst):
		logger('util').debug("batch instantiating from %d data objects",len(data_lst))
		self._load_batch_values(data_lst)

		# fetch, aggregate, and pre-initialize slot data
		slotdata_rows = self.c.impl.Slot.objects.filter(head__in=[di.pk for di in data_lst]).values_list('slot','head_id').distinct()
		slotdata = defaultdict(set)
		for slot in slotdata_rows:
			slotdata[slot[-1]].add(slot[0])
		ret = [self.c.impl.Singular(self,x,msg="deserialize",_slots=slotdata[x.pk]) for x in data_lst]
		logger('util').debug("batch instantiation complete /%d",len(data_lst))
		return ret

class DjangoValueList(list):
	def __init__(self,dataparent,*args,**kwargs):
		super(DjangoValueList,self).__init__(*args,**kwargs)
		self.dataparent = dataparent
	def append(self, item):
		super(DjangoValueList,self).append(item)
		self.dataparent.append_value(item)

def drop_deleted_from_lst(lst,theo,use_logger,in_cache=True,header=None):
	use_logger.debug("checking for deleted items in %r",header if header else lst)
	use_logger.debug("in_cache: %s",in_cache)
	if isinstance(lst,Multiple): lst = [lst]
	for m in lst:
		ndeleted = 0
		of = len(m)
		iters=0
		for x in m[:]: # have to iter through a copy here bc uncache modifies m
			iters+=1
			if not isinstance(x,Singular):
				use_logger.warning("skipping non-Singular value in %s %r",m.name,x)
				continue
			use_logger.debug("- %s[%s]",x.name,x.data.pk)

			try:
				x.value
				if isinstance(x.value,Singular):
					use_logger.debug('-   value.data dict: %r',x.value.data.__dict__)
					if hasattr(x.value.data,'DELETED'):
						use_logger.info("removing deleted value %s=%s",x.name,x.value.name)
						ndeleted+=1
						if (not in_cache) or \
							(isinstance(m,Multiple) and ((x.name,m.head) not in theo.KB)):
							m.remove(x)
						else:
							theo.KB.uncache(key=x.name, pk=x.data.pk) # uncache modifies m
						x.delete()
					else:
						try:
							if not x.value.is_tmp():
								x.value.data.refresh_from_db()
								x.value.data.value
							else:
								use_logger.debug("-   tmp node")
						except ObjectDoesNotExist:
							use_logger.warning("%s[%s] not marked deleted but value does not exist. removing.",x.name, x.data.pk)

							ndeleted+=1
							if (not in_cache) or \
									(isinstance(m,Multiple) and ((x.name,m.head) not in theo.KB)):
								use_logger.debug("-   remove from list")
								m.remove(x)
							else:
								use_logger.debug("-   uncache")
								theo.KB.uncache(key=x.name, pk=x.data.pk) # uncache modifies m
								if x.data.pk:
									x._value_is_init=False
									x.delete()
				else:
					if not x.is_tmp(): x.data.refresh_from_db() # ouch
					use_logger.debug("-   value: %s %s",type(x.value),x.value)
			except EmptyValue:
				use_logger.info("removing item with no remaining values %s[%s]",x.name,x.data.pk)
				ndeleted+=1
				if (not in_cache) or \
						(isinstance(m,Multiple) and ((x.name,m.head) not in theo.KB)):
					m.remove(x)
				else:
					theo.KB.uncache(key=x.name, pk=x.data.pk) # uncache modifies m
				x.delete()
			except Exception as e:
				use_logger.info("removing item %s[%s] with other issue:",x.name,x.data.pk)
				use_logger.error(e)
				ndeleted+=1
				if (not in_cache) or \
						(isinstance(m,Multiple) and ((x.name,m.head) not in theo.KB)):
					m.remove(x)
				else:
					theo.KB.uncache(key=x.name, pk=x.data.pk) # uncache modifies m
				x.delete()
		use_logger.debug("%d deleted of %d items",ndeleted,of)
		assert iters==of,"Fewer loop iterations than list length"


_NO_THEO_VALUE_S = repr(NO_THEO_VALUE)
class DjangoTheoData(mdl.Model):
	DELETE_QUEUE = set() # ugh
	name=mdl.TextField()
	root=mdl.BooleanField(default=False)
	updated=mdl.DateTimeField(auto_now=True)
	# values: related_object from DjangoTheoValue
	def __init__(self,*args,**kwargs):
		super(DjangoTheoData,self).__init__(*args,**kwargs)
		self._value=None
		self._value_is_init=False
	def init_value(self,force=False,values=None):
		ll = logger(f'value/{self.name}')
		assert self.pk, "Attempting to load value from db on temporary data node %s\n%r"%(self.name,self.__dict__)
		ll.debug("[%s] %s: loading value from db",self.pk,self.name)
		assert force or not self._value_is_init, "Already initialized DjangoTheoData[%s] %s" % (self.pk, str(self))
		sql_cursor = len(connection.queries)
		llsql = logger('sql')
		llsql.debug('begin init_value DTD[%s] %s',self.pk,self.name)
		if values is None:
			values = list(self.values.all())
		if len(values) == 0:
			p = self.pk
			ll.info("[%s] %s: empty value; delete me",self.pk,self.name)
			raise EmptyValue(self, "[%s] %s"%(p,self.name))
		def convert(v):
			if v.value_f is None and not (v.value_c is None):
				if v.value_c == _NO_THEO_VALUE_S:
					return NO_THEO_VALUE
				elif v.value_c == '[]':
					return []
				else: return v.value_c
			elif not (v.value_f is None):
				return v.value_f
			else:
				return None

		converted = [convert(v) for v in values]

		if len(converted) == 1:
			self._value = converted[0]
			ll.debug("[%s] %s: value = top elt %s",self.pk,self.name,self._value)
		else:
			self._value = DjangoValueList(self, [x for x in converted if x != []])
			ll.debug("[%s] %s: value = lst [%s...]",self.pk,self.name,self._value[0] if len(self._value)>0 else "")
		self._value_is_init = True
		# if llsql.isEnabledFor(logging.DEBUG):
		# 	for q in connection.queries[sql_cursor:]:
		# 		llsql.debug(q)
		llsql.debug('end init_value DTD[%s] %s',self.pk,self.name)
	@property
	def value(self):
		if not self._value_is_init:
			self.init_value()
		return self._value
	@value.setter
	def value(self, value):
		logkey = 'value/%s'%self.name
		logger(logkey).debug("[%s] Setting value to %r",self.pk,value)
		if self._value_is_init:
			logger(logkey).debug("[%s] current _value = %r",self.pk,self._value)
			assert self._value == NO_THEO_VALUE, f"Rewriting extant non-#notheovalue DjangoTheoData.value {self._value} not yet implemented"
			self.values.all().delete()
		assert not isinstance(value,Multiple), "Setting a value to Multiple needs to be done at the Slot level, not the Data level"
		if isinstance(value,list):
			self._valueFactory([])
			value = DjangoValueList(self, [self._valueFactory(v) for v in value])
		else: value = self._valueFactory(value)
		if self.THEO and self.name[self.name.rfind("."):] in self.THEO.c.type_preds:
			assert isinstance(value,list) or value.value_f, "Something tried to set a type predicate (%s) to a non-entity value %s"%(self.name,value)
		self._value = value
		self._value_is_init = True
	def append_value(self, item):
		logkey = 'value/%s'%self.name
		logger(logkey).debug("[%s] Appending %r to current value %r",self.pk,item,self._value)
		# create the new database entry
		self._valueFactory(item)
		# update our cached Python data structure
		self.init_value(force=True)
	def _valueFactory(self,v):
		# if not self.pk: self.save()
		if isinstance(v, DjangoSingular):
			self.depends(DjangoTheoValue(data=self, value_f=v.data))
			return v
		elif isinstance(v, DjangoTheoData):
			self.depends(DjangoTheoValue(data=self, value_f_id=v.pk))
			return v
		elif isinstance(v, TheoConstant):
			self.depends(DjangoTheoValue(data=self, value_c=repr(v)))
			return v
		elif isinstance(v, TheoItem):
			assert False, "I need a DjangoSingular, a constant, or a stringable here and you gave me a %s" % type(v)
		elif v==None:
			self.depends(DjangoTheoValue(data=self))
		elif isinstance(v,list) and not v:
			self.depends(DjangoTheoValue(data=self, value_c='[]'))
		else:
			self.depends(DjangoTheoValue(data=self, value_c=str(v)))
			return str(v)
	def __str__(self):
		try:
			return "(%s[%s] %s)" % (self.name, self.pk, str(self.value))
		except AssertionError:
			logger('db.data.__str__').error("%s: %r",type(self),self.__dict__)
			raise
	def depends(self, item):
		if not hasattr(self,'_dependents'):
			self._dependents = []
		self._dependents.append(item)
	def save(self, *args, **kwargs):
		save_dependents = not self.pk
		ret = super(DjangoTheoData,self).save(*args,**kwargs)
		logger('db.ids').info("[%s] %s",self.pk,self.name)
		if save_dependents and hasattr(self,"_dependents"):
			batch = defaultdict(list)
			logger('db').info("Saving dependents for %s[%s] %s",self.__class__.__name__,self.pk,self.name)
			for d in self._dependents:
				if isinstance(d,DjangoTheoValue):
					if d.data == self: d.data_id=self.pk
					if d.value_f and not d.value_f.pk:
						d.value_f.depends(d)
						continue
					logger('db').debug(f"Saving dependent Value {d.data_id}:{d.data.name}/{d.value_f_id}/{d.value_c}")
				elif isinstance(d,DjangoTheoSlot):
					if d.head == self:
						d.head_id=self.pk
						if not d.tail.pk:
							d.tail.depends(d)
							continue
					elif d.tail == self:
						d.tail_id=self.pk
						assert d.head.pk, f"Depends loop from {self.pk} at {d}"
					logger('db').debug(f"Saving dependent Slot {d.head_id}/{d.tail_id}")
				batch[type(d)].append(d)#d.save()
			for t in batch: # this doesn't help as much as I'd hope :(
				t.objects.bulk_create(batch[t])
		return ret
	@classmethod
	def builder(cls, value=None, save=False, **kwargs):
		d = cls(**kwargs)
		d.value = value
		logger('db').info("creating new %s %s[%s] %s","saved" if save else "temporary",cls.__name__,d.pk, d.name)
		# if save and d.name == "Alarm_automated-tests.LiaThingLexicalForm":
		# 	logger('db').debug("%s: %r",d.name,traceback.extract_stack(limit=6))
		if save: d.save()
		return d

@receiver(pre_delete,sender=DjangoTheoData)
def manual_cascade_delete_data(sender, **kwargs):
	self = kwargs['instance']
	if self.pk is None or self.pk in DjangoTheoData.DELETE_QUEUE: return
	DjangoTheoData.DELETE_QUEUE.add(self.pk)
	self.ex_pk = self.pk
	logger('db').warning("Deleting %s[%s] %s",self.__class__.__name__,self.pk,self.name)
	if self._value_is_init: logger('db').debug  ("         value %s",self.value)
	result = self.THEO.KB.uncache(key=self.name, pk=self.pk)
	logger('db').debug("Uncache effect: %x",result)
	n=0
	for s in DjangoTheoSlot.objects.filter(head=self.pk).values('slot','tail','tail__name'):
		if s['tail__name']==f"{self.name}.{s['slot']}":
			logger('db').warning("(CASCADE) Uncaching DjangoTheoData[%s] %s",s['tail'],s['tail__name'])
			self.THEO.KB.uncache(key=s['tail__name'], pk=s['tail'])
			n+=1
	logger('db').debug("(CASCADE) uncached %d slots as head %s[%s] %s",n,self.__class__.__name__,self.pk,self.name)
	as_tail = DjangoTheoSlot.objects.filter(tail=self.pk)
	logger('db').debug("(CASCADE) deleting %d slots as tail %s[%s] %s",as_tail.count(),self.__class__.__name__,self.pk,self.name)
	as_tail.delete()
	# logger('db').warning("(CASCADE) Deleting DjangoTheoData[%s] %s",s['tail'],s['tail__name'])
	# self.THEO.KB.uncache(name=s['tail__name'],pk=s['tail'])
	self.DELETED=True
@receiver(post_delete,sender=DjangoTheoData)
def manual_cdd_cleanup(sender, **kwargs):
	self = kwargs['instance']
	DjangoTheoData.DELETE_QUEUE.remove(self.ex_pk)
	logger('db').info("Deletion complete %s[%s] %s",self.__class__.__name__,self.ex_pk,self.name)#,self.__dict__)
	still_has = self.name in self.THEO.KB
	logger('db').debug("                %s in KB %s: %s",self.name,hex(id(self.THEO.KB)&0xffffffff),still_has)
	if still_has:
		still = self.THEO.KB[self.name]
		if still==NO_THEO_VALUE:
			logger('db').debug("                NO_THEO_VALUE")
		else:
			logger("db").debug("                %s[%r]")
			logger("db").debug("                %s[%r]",self.name,still[0].data.pk if isinstance(still[0],DjangoSingular) else "len %s %s"%(len(still),len(still[0])))

class DjangoTheoValue(mdl.Model):
	data=mdl.ForeignKey(DjangoTheoData, on_delete=mdl.CASCADE, related_name='values')
	value_c=mdl.TextField(null=True,blank=True)
	value_f=mdl.ForeignKey(DjangoTheoData,null=True,blank=True, on_delete=mdl.CASCADE, related_name='+')

class DjangoTheoSlot(mdl.Model):
	DELETE_QUEUE = set() # ugh
	head=mdl.ForeignKey(DjangoTheoData, null=False, on_delete=mdl.CASCADE, related_name='+')
	slot=mdl.TextField()
	tail=mdl.ForeignKey(DjangoTheoData, null=True, blank=True, on_delete=mdl.CASCADE, related_name='+')
	updated=mdl.DateTimeField(auto_now=True)
	def __str__(self):
		try:
			return "[%s] - %s -> %s" % (self.head.pk, self.slot, str(self.tail))
		except AssertionError:
			logger('db.slot.__str__').error("%s: %r",type(self.tail),self.tail.__dict__)
			raise
	@classmethod
	def builder(cls, save=False, **kwargs):
		head = kwargs['head']
		kwargs['head'] = head.data
		s = cls(**kwargs)
		if cls.BATCH:
			logger('db').info("scheduling new %s[%s] %s" ,cls.__name__, s.pk, s)
			cls.add(head, s)
		else:
			try:
				logger('db').info("creating new %s %s[%s] %s", "saved" if save else "temporary", cls.__name__, s.pk, s)
			except AssertionError:
				logger('db.slot.builder').error("s.tail: %s %r",type(s.tail),s.tail.__dict__)
				logger('db.slot.builder').error("args.tail: %s %r",type(kwargs['tail']),kwargs['tail'].__dict__)
				raise
			except EmptyValue as e:
				logger('db.slot.builder').error("%s %s[%s]","saved" if save else "temporary", cls.__name__,s.pk)
				logger('db.slot.builder').error("s.tail: %s %r",type(s.tail),s.tail.__dict__)
				logger('db.slot.builder').error("args.tail: %s %r",type(kwargs['tail']),kwargs['tail'].__dict__)
				raise
			if save:
				if s.head.pk and s.tail.pk:
					try:
						s.save()
					except IntegrityError as e:
						logger('db').error("Trouble saving %s %s[%s] %s->%s",cls.__name__,s.slot,s.pk,s.head_id,s.tail_id)
						logger('db').error(e)
						logger('db')
						raise
				elif not s.head.pk:
					logger('db').debug("Deferring save to head elt %s",s.head.name)
					s.head.depends(s)
				elif not s.tail.pk:
					logger('db').debug("Deferring save to tail elt %s",s.tail.name)
					s.tail.depends(s)
		return s
	BATCH=False # ham-handed transactions lite: terrible but 30% faster
	_BATCH={}
	@classmethod
	def add(cls, head, item):
		key = item.tail.name
		if key not in cls._BATCH:
			cls._BATCH[key] = (list(), DjangoTheoData.THEO.c.impl.Multiple(DjangoTheoData.THEO,key,[],head=head))
		cls._BATCH[key][0].append(item)
		cls._BATCH[key][1].insert(0, # avoid custom append() functionality
			DjangoTheoData.THEO.c.impl.Singular(
				DjangoTheoData.THEO,
				item.tail,
				msg='deserialize'))
	@classmethod
	def in_batch(cls,key):
		if key not in cls._BATCH: return
		return cls._BATCH[key]
	@classmethod
	def finish_batch(cls):
		cls.objects.bulk_create(chain(*[x[0] for x in cls._BATCH.values()]))
		cls._BATCH={}
		cls.BATCH=False

@receiver(pre_delete,sender=DjangoTheoSlot)
def manual_cascade_delete_slot(sender,**kwargs):
	self = kwargs['instance']
	if self.pk is None or self.pk in DjangoTheoSlot.DELETE_QUEUE: return
	if self.tail.pk is None or self.tail.pk in DjangoTheoData.DELETE_QUEUE: return
	DjangoTheoSlot.DELETE_QUEUE.add(self.pk)
	self.ex_pk = self.pk
	logger('db').warning("  Deleting %s[%s] %s->%s->%s",self.__class__.__name__,self.pk,self.head.name,self.slot,self.tail.name)
	if self.tail.name == f"{self.head.name}.{self.slot}":
		self.tail.delete()
@receiver(post_delete,sender=DjangoTheoSlot)
def manual_cds_cleanup(sender, **kwargs):
	self = kwargs['instance']
	if hasattr(self,'ex_pk'): DjangoTheoSlot.DELETE_QUEUE.remove(self.ex_pk)

class DjangoTheoItem(TheoItem):
	def __init__(self, theo):
		self._theo = theo
	@property
	def theo(self): return self._theo
	@theo.setter
	def theo(self, t): self._theo = t

class DjangoSingular(Singular,DjangoTheoItem):
	def __init__(self, theo, name, value=NO_THEO_VALUE, tmp=True, msg="new", _slots=None, *slot_tuples, **slot_kwargs):
		DjangoTheoItem.__init__(self, theo)
		self._value_is_init = False
		self._value = None
		self._slots_is_init = False
		self._slots = None
		self._fresh = None
		self._tmp_slotvalues=DjangoTheoCache(self.theo,"tmpcache")
		if isinstance(_slots,tuple):
			slot_tuples = list(slot_tuples)
			slot_tuples.insert(0,_slots)
			_slots = None
		if isinstance(msg,tuple):
			if not isinstance(slot_tuples,list): slot_tuples = list(slot_tuples)
			slot_tuples.insert(0,msg)
			msg="new"
		if msg == "new":
			logger('DjangoSingular').info("new %s %s","temporary" if tmp else "saved", name)
			if tmp and name=="katie.university affiliation.LiaThingLexicalForm":
				logger('DjangoSingular').info("from:\n%s",stackTrace())
				logging.getLogger("pytheo.theo.cmd.slot_set").setLevel(logging.DEBUG)
			assert isinstance(value,TheoItem) or str(value) != "", "Bad blank value %r"%value
			self.data = self.theo.c.impl.Data.builder(name=name, value=value, save=not tmp)
			self.set_logkey()
			self._add_to_kb()
			# self._slots = set()
			n = len(slot_tuples)+len(slot_kwargs)
			logger(self.logkey).debug("%d slots",n)
			slots = self.theo.slots(self, tmp, *slot_tuples, **slot_kwargs)
			if slots:
					for k,v in slots:
						try:
							m = self.slot_set(k,v)
							m.head = self
						except ValueError:
							logger().error("%s=%s",k,v)
							raise
			elif n>0: logger().error("%s: no results from theo.slots on %d slots",self.name, n)
		elif msg == "deserialize":
			self.data = name
			self.set_logkey()
			self._init_slots(_slots)
		else: assert False, "Bad initialization message %s" % msg
	@property
	def name(self): return self.data.name
	# @name.setter
	# def name(self,n):
	# 	self.data.name = n
	# 	self.data.save()
	def _init_value(self):
		if isinstance(self.data.value,self.theo.c.impl.Data):
			self._value = self.theo.c.impl.Singular(self.theo,self.data.value,msg='deserialize')
		else:
			if not isinstance(self.data.value,(DjangoSingular,str,TheoConstant)):
				logger(self.logkey).debug("_init_value: %s %s not a %s",type(self.data.value),self.data.value,self.theo.c.impl.Data)
			self._value = self.data.value
		self._value_is_init = True
	@property
	def value(self):
		if not self._value_is_init:
			self._init_value()
		return self._value
	@value.setter
	def value(self, v):
		self.data.value = v
		self._init_value()
	def _init_slots(self,slots=None):
		logger(self.logkey).debug("initializing slots from %s","hint" if slots else "scratch")
		# if self.name == "Alarm_automated-tests":
		# 	import traceback
		# 	ll = logger(f'DjangoSingular.{self.name}')
		# 	for r in traceback.format_stack():#print_list(traceback.extract_stack()):
		# 		ll.debug(r)
		if slots is None:
			slots = []
			if self.data.pk:
				slots.extend(x for x in self.theo.c.impl.Slot.objects.filter(head=self.data).values_list('slot', flat=True).distinct())
			slots.extend(x for x in self._tmp_slotvalues.keys())
			slots = set(slots)
		self._slots = slots
		self._slots_is_init = True
		logger(self.logkey).debug("%d slots known",len(self._slots))
	@property
	def slots(self):
		if not self._slots_is_init: self._init_slots()
		return self._slots
	def is_tmp(self): return self.data.pk is None
	def set_logkey(self):
		self.logkey = f"DjangoSingular.{self.name}[{self.data.pk}]"
	def has_slot(self, slot):
		is_new = not self._slots_is_init
		local = slot in self.slots
		if local:
			# we never call has_slot in pytheo without a more-or-less immediate call to raw_slot_get,
			# so we lose nothing by explicitly checking our cached value is current
			check = self.raw_slot_get(slot)
			if check==NO_THEO_VALUE:
				logger(self.logkey).debug("[%s] has_slot %s: true locally + cache miss => removed spurious entry; returning False",
										  self.data.pk,slot)
				if slot in self.slots:
					# this guard shouldn't be necessary but it's coming up as a key error sometimes so ???
					self.slots.remove(slot)
				return False
			logger(self.logkey).debug("[%s] has_slot %s: true locally (of %d slots)",self.data.pk,slot,len(self.slots))
			return True
		fqname = f"{self.name}.{slot}"
		remote = (fqname,self) in self.theo.KB
		if remote and is_new:
			check = self.theo.KB[fqname]
			drop_deleted_from_lst(check,self.theo,logger(self.logkey))
			logger(self.logkey).warning("Spurious slot value: %s",check)
			logger(self.logkey).warning("Head: %s",check.head)
			logger(self.logkey).warning("Self.data: %s",self.data)
		assert not (remote and is_new), f"{fqname} is listed in the KB ({check}) but didn't appear in our ({self.short_str()}) DB query results. KB caching error?"
		if remote:
			logger(self.logkey).debug("slot %s found remotely; refreshing slots list from db",slot)
			self._init_slots()
		logger(self.logkey).debug("[%s] has_slot %s: %s via KB",self.data.pk,slot,remote)
		return remote
	def raw_slot_set(self, slot, item):
		ll = logger(f"{self.logkey}.raw_slot_set")
		if item == None:
			ll.debug("Removing slot %s entirely",slot)
			self.theo.c.impl.Slot.objects.filter(head=self.data, slot__exact=slot).delete()
			logger(f"{self.logkey}.slots").debug("removing %s",slot)
			self._slots.remove(slot)
			# do we need to do an uncache here in the KB?
			return
		if isinstance(item,Multiple) and item.head == None:
			ll.debug("Multiple %s missing head; setting to self",item.name)
			item.head=self
		items = item if isinstance(item,Multiple) else [item]
		for i in items:
			assert not (isinstance(i,Multiple) and i.head == None), f"uninitialized head in subitem Multiple: {self.logkey}.raw_slot_set {slot} {i} in {items}"
			if self.data.pk and not i.data.pk:
				i.data.save()
				i.set_logkey()

		# use Theo object model
		current_slot_value = self.raw_slot_get(slot,insert=False)
		if current_slot_value == NO_THEO_VALUE:
			ll.debug("No pre-existing value for %s",slot)
			for i in items:
				ll.debug("%ssaving %s.%s","" if self.data.pk else "not ",self.name,slot)
				try:
					self.theo.c.impl.Slot.builder(head=self, slot=slot, tail=i.data, save=self.data.pk)
				except AssertionError:
					# ll.error("head: %s %r",type(self.data), self.data.__dict__)
					ll.error("tail: %s %r",type(i.data), i.data.__dict__)
					raise
				except EmptyValue:
					ll.error("tail: %s %r",type(i.data), i.data.__dict__)
					raise
			logger(f'{self.logkey}.slots').debug("adding %s",slot)
			self._slots.add(slot)
			if self.data.pk:
				self.theo.KB[(item.name,self)] = item
			else: self._tmp_slotvalues[item.name] = item
			return item
		pks = sorted([(i.data.pk,i) for i in items],key=lambda x:x[0])
		has = sorted([(v.data.pk,v) for v in current_slot_value],key=lambda x:x[0])
		if current_slot_value.head is None:
			if ll.isEnabledFor(logging.DEBUG): ll.debug("current slot value %s missing head; setting to self",current_slot_value.short_str())
			current_slot_value.head = self
		remove = set()
		vi=0
		for pk,it in pks:
			while vi < len(has) and pk > has[vi][0]:
				remove.add(has[vi][0])
				current_slot_value.remove(has[vi][1])
				vi+=1
			if vi >= len(has) or pk != has[vi][0]:
				current_slot_value.append(it)
			else: vi += 1
		if self.data.pk: self.theo.c.impl.Slot.objects.filter(pk__in=remove).delete()
		return current_slot_value

	def raw_slot_get(self, slot, insert=False):
		key = (f"{self.name}.{slot}",self)
		ll = logger(f"{self.logkey}.raw_slot_get:{slot}")
		lmeta = logger("DjangoSingular.raw_slot_get.status")
		lmeta.debug("getting %s",slot)
		ll.debug("begin (self:%s) (fresh:%s)","tmp" if self.is_tmp() else "stored",self._fresh)
		ret=False,False
		if key[0] in self._tmp_slotvalues:
			ret = self._tmp_slotvalues[key[0]], 'tmp'
		elif (not self.is_tmp()) and (key in self.theo.KB.cache):
			ret = self.theo.KB[key], 'KB'
		if ret[0]:
			assert ret[0] != NO_THEO_VALUE, "Bad %s cache: reported %s present but returned NO_THEO_VALUE" % (ret[1],key)
			if ret[0].head != self:
				ll.warning("head from %s cache is %r not self; reassigning",ret[1],ret[0].head)
				if ret[0].head is not None:
					ll.warning("Unexpected head is not None: %s %r",ret[0].head.data.pk, ret[0].head.data.__dict__)
					ll.warning("Replacing with: %s %r",self.data.pk, self.data.__dict__)
				ret[0].head = self
			if self._fresh != slot: drop_deleted_from_lst(ret[0],self.theo,ll)
			if ret[0]:
				ll.debug("returning cached slot from %s with %d values",ret[1],len(ret[0]))
				lmeta.debug("returning cached slot from %s with %d values",ret[1],len(ret[0]))
				return ret[0]
		if self.is_tmp() or ret[-1]:
			if not insert:
				ll.debug("returning NO_THEO_VALUE")
				lmeta.debug("returning NO_THEO_VALUE")
				return NO_THEO_VALUE
			ret = []
		elif self.theo.c.impl.Slot.BATCH:
			ret = self.theo.c.impl.Slot.in_batch(key)
			if ret:
				_,ret = ret
				ll.debug("returning Multiple from slot batch: %r",ret)
				lmeta.debug("returning Multiple from slot batch")
				self._tmp_slotvalues[key[0]] = ret
				return ret
			if not insert:
				ll.debug("returning NO_THEO_VALUE")
				lmeta.debug("returning NO_THEO_VALUE")
				return NO_THEO_VALUE
			ret = []
		else:
			ret = self.theo.KB.dj_prefetch(self,[key[0]])
			if ret[0] != NO_THEO_VALUE:
				ll.debug("returning prefetched slot with %d values",len(ret[0]))
				lmeta.debug("returning prefetched slot with %d values",len(ret[0]))
				return ret[0]
			else: ret = []
		if not ret:
			if not insert:
				ll.debug("returning NO_THEO_VALUE")
				lmeta.debug("returning NO_THEO_VALUE")
				return NO_THEO_VALUE
			ll.debug("inserting empty Multiple")
			lmeta.debug("inserting empty Multiple")
		lst = list()
		for x in ret:
			if isinstance(x.tail,self.theo.c.impl.Data):
				tail = self.theo.KB.dj_convert(x.tail)
				try:
					tail.value
				except EmptyValue:
					ll.debug("deleting Empty Value ghost slot %s",tail.name)
					self.theo.KB.uncache(key=tail.name, pk=tail.pk)
					tail.delete()
					continue # skip slots whose values have been deleted; they're ghosts
				# lst.append(tail)
				lst.append(self.theo.c.impl.Singular(self.theo,tail,msg='deserialize')) # consider caching here
			elif isinstance(x.tail, DjangoTheoData):
				assert False, f"x.tail is DjangoTheoData but is not theo.c.impl.Data (={self.theo.c.impl.Data})\n{x}\n{x.tail}"
			#"How did a DjangoTheoData get into tail? \n%r\n%r"%(x,x.tail)
			else: assert False, "shouldn't happen?" #lst.append(x.tail)
		# lst = self.theo._deserialize_batch(lst) # <-- this actually results in more DB queries so don't do it
		ll.debug("returning Multiple %r",lst)
		lmeta.debug("returning Multiple from slots")
		m = self.theo.c.impl.Multiple(
			self.theo,
			key[0],
			lst,
			head=self)
		if self.is_tmp():
			self._tmp_slotvalues[key[0]] = m
		else:
			self.theo.KB[key] = m
		return m
	def _set_fresh(self,slot):
		logger(f'{self.logkey}._fresh').debug("Setting _fresh (%s was %s)",slot,self._fresh)
		self._fresh = slot
	def _reset_fresh(self,fun,*args,**kwargs):
		ret = fun(*args,**kwargs)
		logger(f'{self.logkey}._fresh').debug("Resetting _fresh (%s)",self._fresh)
		self._fresh = None
		return ret
	def slot_set(self,attr,val):
		ret = self._reset_fresh(super(DjangoSingular,self).slot_set,attr,val)
		if attr in {self.theo.c.preds.supertype,self.theo.c.preds.subtype}:
			for v in ret:
				v.value._reset_fresh(lambda:0)
		return ret
	def slot_get(self,slot):
		return self._reset_fresh(super(DjangoSingular,self).slot_get,slot)
	def slot_delete(self,*args,**kwargs):
		return self._reset_fresh(super(DjangoSingular,self).slot_delete,*args,**kwargs)
	def _raw_slot_get(self, slot):
		ret = super(DjangoSingular,self)._raw_slot_get(slot)
		self._set_fresh(slot)
		return ret
	def _raw_slot_set(self, attr, item):
		ret = super(DjangoSingular,self)._raw_slot_set(attr, item)
		self._set_fresh(attr)
		return ret
	def _has_slot(self, slot):
		ret = super(DjangoSingular,self)._has_slot(slot)
		self._set_fresh(slot)
		return ret

	def apply_methods(self, slot):
		ll = logger(override="pytheo.theo.cmd.apply_methods.djpytheo")
		ll.debug("%s.%s apply_methods (dj prefetching)",self.name,slot)

		se = self.theo.fetch(slot) # prefetch the root definition for this slot
		if NO_THEO_VALUE == se:
			ll.debug("%s.%s apply_methods returning n/a (slot entity is NO_VALUE")
			return
		# if not isinstance(se[0],Singular):
		# 	for s in se:
		# 		if isinstance(s,Multiple):
		# 			for si in s:
		# 				si.data.refresh_from_db()

		prefetch = self.theo.KB.dj_prefetch(se[0],[
				f"{slot}.availableMethods",
				f"{slot}.domain",
				f"{slot}.generalizations",
				f"{slot}.defaultValue",
				f"{slot}.whenToCache"
			])
		ret = super(DjangoSingular,self).apply_methods(slot,slot_entity=se[0])
		return ret
	def address(self):
		return self.data.pk
	def delete(self):
		if self.is_tmp(): return
		parent = self.name.find(".")
		parent = parent>0 and self.theo.fetch(self.name[:parent])
		ex_pk = self.data.pk
		self.data.delete()
		if parent:
			for p in parent:
				p._init_slots()
		assert self.data.pk is None
	def __hash__(self):
		if self.is_tmp():
			return super(DjangoSingular,self).__hash__() # oof
		return hash(self.data)
	def __eq__(self, other):
		if not isinstance(other,DjangoSingular): return False
		return self.data.pk == other.data.pk
	def __ne__(self, other): return not self.__eq__(other)
	def str(self,*args,**kwargs):
		ret = super(DjangoSingular,self).str(*args,**kwargs)
		return ret.replace(",",f"[{self.data.pk}],",1)
	def short_str(self):
		ret = super(DjangoSingular,self).short_str()
		if ret.find("=")>0: return ret.replace("=",f"[{self.data.pk}]=",1)
		return ret.replace(")",f"[{self.data.pk}])",1)
	def __repr__(self):
		ret = super(DjangoSingular,self).__repr__()
		return ret.replace("=",f"[{self.data.pk}]=",1)


class DjangoMultiple(Multiple,DjangoTheoItem):
	def __init__(self, theo, name, lst, head=None):
		DjangoTheoItem.__init__(self, theo)
		self._name = name
		self._slot = name
		delim = name.rfind(".")
		if delim >= 0:
			self._slot = name[delim+1:]
		Multiple.__init__(self, lst, head)
	@property
	def name(self): return self._name
	@name.setter
	def name(self,n): self._name = n
	def append(self, item):
		Multiple.append(self,item)
		incomplete = self.head == None
		assert not incomplete, "Can't add slot value to %s without knowing the head element"%self.name
		assert isinstance(self.head,self.theo.c.impl.Singular), "Bad head %r"%self.head
		assert isinstance(item,self.theo.c.impl.Singular), "Bad item %r"%item
		assert self.head.data.pk, f"{self.name} Unsaved head {self.head.data}"
		s = self.theo.c.impl.Slot.builder(head=self.head, slot=self._slot, tail=item.data, save=self.head.data.pk)
	def remove(self,item):
		list.remove(self,item)
