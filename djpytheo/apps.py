# -*- coding: utf-8 -*-


from django.apps import AppConfig


class DjpytheoConfig(AppConfig):
    name = 'djpytheo'
