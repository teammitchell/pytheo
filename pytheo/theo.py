import itertools
import traceback
from collections import defaultdict
from types import GeneratorType

import config
import os
import shelve
from itertools import chain
import logging
from functools import reduce
def logger(suffix=None):
	name = __name__
	if suffix: name += "." + suffix
	return logging.getLogger(name)


class TheoException(Exception):
	def __init__(self, msg):
		Exception.__init__(self, msg)


class TheoConstant(object):
	_HASH=hash("TheoConstant")
	def __init__(self, name):
		self.name = name

	def __str__(self):
		return "#%s" % self.name

	def __repr__(self):
		return "TheoConstant(%s)" % self.name

	def __eq__(self, other):
		if not isinstance(other,TheoConstant): return False
		return self.name == other.name

	def short_str(self):
		return str(self)

	def __hash__(self):
		return (self._HASH ^ hash(self.name))


def flatten(lst):
	def update(accum,item):
		try:
			return accum+item
		except TypeError:
			print("accum",type(accum),accum)
			print("item",type(item),item)
			raise
	return reduce(update, lst, [])

def first(lam, lst):
	for li in lst:
		if lam(li): return li

NO_THEO_VALUE = TheoConstant("NO_THEO_VALUE")

def cis_read_chardata(name):
	return config.ConfigInputStream(open(name, 'r'))

class Theo(object):
	class Namespace(object):
		"""python.config requires a specific namespace for class lookup"""

		def __init__(self):
			for k, v in list(globals().items()):
				if isinstance(v, type):
					setattr(self, k, v)

		def __repr__(self):
			return "<Namespace('%s')>" % ','.join(list(self.__dict__.keys()))



	@staticmethod
	def configuration(path=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'defaults.config')):
		# config.streamOpener = cis_read_chardata
		return config.Config(path)

	def __init__(self, config=None):
		self.c = config if config else Theo.configuration()
		ns = Theo.Namespace()
		self.c.addNamespace(ns)
		logger().debug("added namespace %r",ns)
		logger().debug("Theo configuration:")
		for k in self.c:
			logger().debug("  %s=%s",k,str(self.c[k]))
		self.PRIMITIVES = None

	def _pre_load(self):
		self.KB = TheoCache(self, "kb") if self.c.storage == "memory" else ShelveTheoCache(self, "kb", self.c.storage)

	def load_kb(self):
		logger().debug("loading kb")
		self._pre_load()
		self.PRIMITIVES = self.c.primitives
		if self.c.root.entity not in self.KB:
			logger().info("creating root entities")
			self._create_root_entities()

	def _create_root_entities(self):
		self.c.impl.Singular(self, self.c.root.entity, tmp=False)
		self.Entity(self.c.root.relation, self.KB[self.c.root.entity])
		self.Slot(self.c.preds.subtype)
		self.Slot(self.c.preds.supertype)

	def wrap_value(self, slot, value_args, pluralize=True, tmp=True, head=None, **kwargs):
		if not isinstance(slot,tuple): slot=(slot,)
		value, rest = (value_args[0], value_args[1:]) if isinstance(value_args, tuple) else (value_args, tuple())
		rest += tuple(kwargs.items())
		logkey=f"wrap_value" #.{value}"
		logger(logkey).debug("value, rest: %r, %r",value,rest)
		name = ".".join(slot)
		# dereference addresses if it matters
		if slot[-1] in self.c.type_preds:
			if isinstance(value, str):
				if value not in self.PRIMITIVES:
					ref = self.fetch(value)
					if ref != NO_THEO_VALUE:
						value = ref
					else:
						raise TheoException(
							"Expected a slot address here (value of slot %s) but couldn't find the referent: %r" % (
							slot, value))
		if isinstance(value,TheoItem) and value.name == name:
			_value = value.value # unpack first (we're passing-by-value, not -reference)
			if isinstance(value,Multiple):
				assert len(value)==1, "Not sure how to copy multiple candidate slot values: %s" % value.short_str()
				_value = list(_value)[0]
			value = _value
		if isinstance(value,TheoItem) and value.name != name:
			if isinstance(value,Multiple):
				ret = self.c.impl.Multiple(
					self,
					name,
					[
						self.c.impl.Singular(self,name,v,tmp,*rest)
						for v in value
					],
					head=head)
			else:
				ret = self.c.impl.Singular(self,name,value,tmp,*rest)
		else:
			value_args = (value,) + rest
			logger(logkey).debug("value_args: %r",value_args)
			value = self.c.impl.Singular(self, name, value, tmp, *rest)

			ret = self.c.impl.Multiple(self, name, [value], head=head) if pluralize else value
		logger(logkey).debug("TheoItem: %r",ret)
		return ret

	def slots(self, head, tmp, *entries, **kwargs):
		ret = list()
		if isinstance(head,tuple):
			head_name = ".".join(head)
			head_tuple = head
			head_ent = self.fetch(*head_tuple) # the timing on this might not work
		elif isinstance(head,Singular):
			head_name = head.name
			head_ent = head
			head_tuple = tuple(head_name.split("."))
		else:
			head_name = head
			head_tuple = tuple(head.split("."))
			head_ent = self.fetch(*head_tuple) # the timing on this might not work
		logger("slots").debug("using path %s",head_tuple)
		for e in chain(entries, list(kwargs.items())):
			try:
				k, v = e
				if isinstance(k,tuple):
					name = '.'.join(k)
				else:
					name = k
					k=tuple(k.split("."))

				name = f"{head_name}.{name}"
				k=head_tuple+k

			except ValueError:
				# raise
				raise TheoException("Bad slots format. Need a list of 2-tuples; got %r in %r" % (e, entries))
			else:
				logger('slots').debug("appending %s = %r %r %r",k[-1],k,type(v),v)
				ret.append((
					k[-1],
					(
						v if (isinstance(v, TheoItem) and v.name == name)
						else self.wrap_value(k, v, pluralize= True, tmp=tmp, head=head_ent) # 'pluralize=True' was 'pluralize=(k not in ret)' -- what was this for?
					)
				))
		return ret

	def Entity(self, name, generalization=None, slots=None, standalone=True, **slot_kwargs):
		if standalone: logger("Entity").debug("creating entity: %s",name)
		generalization = generalization if generalization else self.KB[self.c.root.entity]
		assert isinstance(generalization, (TheoItem, str)), "Unexpected type for generalizations %r %r" % (type(generalization), generalization)
		# TODO: this specifies >1 generalization as an array; is that what we actually want?
		# generalization = ((name, self.c.preds.supertype), generalization)
		slots = slots if slots else list()
		slots.insert(0,(self.c.preds.supertype, generalization))
		# [slots.insert(i,x) for i,x in enumerate(self.slots(name, generalization))]
		logger().debug(slots[0])
		# assert not name in self.KB, "creating Entity for pre-existing name %s"%name
		if name in self.KB:
			ent = self.KB[name]
			assert ent != NO_THEO_VALUE, "Unexpected NO_THEO_VALUE @ %s"%name
			assert len(ent) == 1, "Unexpected multi-value Entity @ %s %r"%(name,ent)
			ent = ent[0]
			logger('Entity').warning("merging with existing entity %r",ent)
			for s in slots:
				logger('Entity').warning("  adding slot %r",s)
				ent.slot_set(*s)
		else:
			ent = self.c.impl.Singular(self, name, NO_THEO_VALUE, False, *slots, **slot_kwargs)
			# self.KB[name] = ent
		# supertypes = slots[0][1].value if isinstance(slots[0][1].value,list) \
		# 	else list(slots[0][1].value) if isinstance(slots[0][1],Multiple) \
		# 	else [slots[0][1].value]
		# for s in itertools.chain(*[y.slots for y in supertypes]):
		# 	logger().debug("pre-caching %s %s",ent.name,s)
		# 	v = ent.slot_get(s)
		# 	if v == NO_THEO_VALUE: continue # TODO: why is this here
		if standalone: logger('Entity').info("created entity: %r",ent)
		return ent

	def Slot(self, name, generalizations=None, slots=None, **slot_kwargs):
		logger("Slot").debug("creating slot: %s",name)
		ret = self.Entity(name, generalizations if generalizations else self.KB[self.c.root.relation], slots, standalone=False, **slot_kwargs)
		logger('Slot').info("created slot: %r",ret)
		return ret

	def transitiveClosure(self, entityref, slot, visited=None, exclusions=[], quarry=None):
		""" TODO: rewrite with yield insetad of return """
		if visited == None: visited = list()
		entity = entityref if isinstance(entityref, TheoItem) else self.fetch(entityref)
		if entity == NO_THEO_VALUE:
			if slot not in {self.c.preds.supertype,self.c.preds.subtype}:
				logger().debug("Sorry, I couldn't find %r in my KB",(entityref,))
				assert False
			logger('transitive').debug("%s: end of the line",entity.short_str())
			return visited
		if quarry and (entity == quarry): return [visited[0] if visited else entity,entity]
		if entity in visited:
			logger('transitive').debug("%s: already seen",entity.short_str())
			return visited
		if entity in exclusions:
			logger('transitive').debug("%s: excluded",entity.short_str())
			return visited
		visited.append(entity)
		tail = entity.raw_slot_get(slot)
		if tail != NO_THEO_VALUE:
			logger('transitive').debug("%s: descending %r",entity.short_str(),tail)
			for desc in tail.value:
				if not isinstance(desc, TheoItem):
					logger('transitive').debug("%s: skipping %s %s",entity.short_str(),type(desc),desc)
					continue
				visited = self.transitiveClosure(desc, slot, visited)
		logger('transitive').debug("%s: finished",entity.short_str())
		return visited

	def descendants(self, item, quarry=None):
		subs = self.transitiveClosure(item, self.c.preds.subtype, quarry=quarry)
		if subs: return subs[1:]
		return []

	def ancestors(self, item, quarry=None):
		sups = self.transitiveClosure(item, self.c.preds.supertype, quarry=quarry)
		if sups:
			logger('util').debug("ancestors: %r",sups)
			return sups[1:]
		return []

	def fetch(self, *address):
		key = ".".join(address)
		if key in self.KB:
			logger('fetch').debug("%s in KB",key)
			if self.KB[key]==NO_THEO_VALUE:
				logger('fetch').error("%s in KB but returned NO_THEO_VALUE",key)
			# assert self.KB[key]!=NO_THEO_VALUE,"Bad database: %s in KB but returned NO_THEO_VALUE"%key
			logger('fetch').debug("returning cached address %s (%d values)",key,len(self.KB[key]))
			return self.KB[key]
		return NO_THEO_VALUE


	PRIMITIVE_TYPECAST = {
		'boolean':(bool,),
		'string':(str,),
		'number':(float,int),
		'datetime':(object,), # TODO
		'date':(object,), # TODO
		'time':(object,), # TODO
	}

	def typecheck_domain(self, slot, head):
		return self.typecheck(slot,"domain",head)

	def typecheck_range(self, slot, tail):
		return self.typecheck(slot,"range",tail)

	def typecheck(self, slot, prop, value):
		if not isinstance(slot,Singular):
			slot = self.fetch(slot)
			if slot == NO_THEO_VALUE:
				raise TheoException(f"Can't typecheck {slot}; NO_THEO_VALUE")
			slot = slot[0]
		slot_prop = slot.slot_get(prop)
		if slot_prop == NO_THEO_VALUE:
			# unspecified range accepts any type
			return True
		for r in slot_prop:
			if r.value in self.PRIMITIVES:
				cast = self.PRIMITIVE_TYPECAST[r.value]
				for c in cast:
					if isinstance(value, c): return True
				for c in cast:
					try:
						c(value)
						return True
					except:
						continue
			elif isinstance(value,TheoItem) \
					and isinstance(r.value,Singular) \
					and self.isa(value, r.value): return True
		return False


	def isa(self, item, candidate_type):
		if isinstance(candidate_type,TheoItem):
			candidate_type = candidate_type.name
		logger().debug("is %s a %s?", item.short_str(), candidate_type)
		for c in self.ancestors(item):
			if c.name == candidate_type:
				logger().debug("yes: %s",c.short_str())
				return True
			logger().debug("no: %s",c.short_str())
		return False

	def method(self, name):
		return name in self.c.method_types

	def inherit(self, item, slot):
		ll = logger("method.inherit")
		ll.debug("%s.%s",item.name,slot)
		# using slot_get here to activate inference if needed
		domain_record = self.fetch(slot)[0].slot_get(self.c.preds.domain)[0]
		domain = domain_record.value

		def helper(e, s, d):
			if ((e in d) if isinstance(d,list) else (e == d)):
				return None, "entity == domain %s" % e.short_str()
			gs = e.raw_slot_get(self.c.preds.supertype)
			if gs==NO_THEO_VALUE: return None, "%s.%s = NO_THEO_VALUE"% (e.name, self.c.preds.supertype)
			# gs = list(gs.value)
			gs_list = []
			for g in gs.value:
				if g.has_slot(s):
					return g.raw_slot_get(s),g.short_str()
				gs_list.append(g)
			for g in gs:
				val,src = helper(g, s, d)
				if val != NO_THEO_VALUE: return val,src
			return None,"possibilities exhausted"
		r,src = helper(item, slot, domain)
		ret = list(r.value) if r else None  # TODO: double check we got the right level of indirection here
		ll.debug("%r via %s",r,src)
		ll.debug("%r",ret)
		return ret

	def defaultValue(self, item, slot):
		ll = logger("method.defaultValue")
		ll.debug("%s.%s",item.name,slot)
		ents = self.fetch(slot)
		if ents==NO_THEO_VALUE: return
		for ent in ents:
			if ent.has_slot(self.c.preds.default):
				r = ent.raw_slot_get(self.c.preds.default)
				ll.debug("raw defaultValue: %r",r)
				assert isinstance(r,Multiple), "Unexpected type for defaultValue of %s (should be Multiple): %s"%(ent.short_str(),type(r))
				assert len(r)==1, "Unexpected number of defaultValue values %d != 1 for %s"%(len(r),ent.short_str())
				r = list(r.value)
				ll.debug("%r",r)
				return r#self.wrap_value(f"{item.name}.{slot}", r) if r else None

	def prolog(self, item, slot):
		logger('method.prolog').debug("(%s) Not yet implemented",slot)

	def dropAllContext(self, item, slot):
		ll = logger("method.dropAllContext")
		ll.debug("%s.%s",item.name,slot)
		delim = item.name.rfind(".")
		if delim<0: return
		cfname = item.name[delim+1:]
		if cfname in self.KB:
			for candidate in self.fetch(cfname):
				if candidate != item:
					ret = candidate.slot_get(slot)
					ll.debug("%r",ret)
					if ret != NO_THEO_VALUE:
						return list(ret.value)

	def print_graphviz(self, *seeds):
		system_whitelist = {
			self.c.preds.domain,
			self.c.preds.range,
			self.c.preds.subtype,
			self.c.preds.supertype
		}

		nodes = {}
		inodes = {}
		edges = {}
		iedges = {}
		slots = defaultdict(dict)

		def add_node(n, n_id=None):
			if n in nodes: return True
			if n_id is None:
				n_id = len(nodes)
			nodes[n] = n_id
			inodes[n_id] = n

		def add_edge(e, e_id=None):
			if e in edges: return True
			if e_id is None:
				e_id = len(edges)
			edges[e] = e_id
			iedges[e_id] = e
			h, s, t = e
			slots[s][h] = e

		def walk(node):
			if add_node(node): return
			if node.name == self.c.root.entity: return
			for slot in node.slots:
				if slot == self.c.preds.subtype: continue
				for tail in node.slot_get(slot):
					walk(tail)
					add_edge((node, slot, tail))
					if isinstance(tail.value, Singular):
						walk(tail.value)
						add_edge((tail, "value", tail.value))

		# populate graph
		for seed in seeds:
			if seed not in nodes:
				walk(seed)

		# prune 'generalizations' paths to a single edge
		for edge in slots[self.c.preds.supertype].values():
			(head, _, generalization) = edge
			vedge = slots['value'][generalization]
			(_, _, tail) = vedge
			# remove generalization node
			del inodes[nodes[generalization]]
			del nodes[generalization]
			# remove value edge
			del iedges[edges[vedge]]
			del edges[vedge]
			# remove generalization edge
			edge_id = edges[edge]
			del iedges[edges[edge]]
			del edges[edge]
			# add edge with e_head, 'generalizations', v_tail
			# using old generalizations edge id
			add_edge((head, self.c.preds.supertype, tail), edge_id)

		# build color map
		# colors are from Wong, B. (2011) Points of view: Color blindness. Nature Methods 8:441.
		colors = "#e69f00 #56b4e9 #009e73 #0072b2 #d55e00 #cc79a7 #f0e442".split()
		styles = "solid dashed bold dotted".split()
		color = {}
		style = {}
		ci = 0
		si = 0
		for slot in slots:
			if slot == self.c.preds.supertype:
				color[slot] = "grey75"
				style[slot] = "solid"
			else:
				color[slot] = colors[ci]
				style[slot] = styles[si]
				ci += 1
				if ci >= len(colors):
					si += 1
					ci = 0
				assert si < len(styles), \
					f"Number of custom edges exceeds {len(colors) * len(styles)}; go grab a larger color palette"

		# ======================== formatting below
		print("digraph G {")
		print("rankdir=\"LR\";")
		print("node [shape=record];")
		for n, ni in nodes.items():
			if isinstance(n.value, Singular) or n.value == NO_THEO_VALUE:
				print(f"node_{ni} [label=\"{{{'|'.join(n.name.split('.'))}}}\"];")
			else:
				print(f"node_{ni} [label=\"{{{'|'.join(n.name.split('.'))}}}|{n.value}\"];")

		# print('#'*30)

		for e, ei in edges.items():
			head, slot, tail = iedges[ei]
			print(f"node_{nodes[head]} -> node_{nodes[tail]} "
				  f"[label=\"{slot}\"; "
				  f"color=\"{color[slot]}\"; fontcolor=\"{color[slot]}\"; "
				  f"style=\"{style[slot]}\"];")  # {e}:\t{head.short_str()} ->{slot} ->{tail.short_str()}")
		print("}")

def stackTrace(limit=8):
	return "".join(str(x) for x in traceback.format_stack(limit=limit))

class TheoCache(dict):
	def __init__(self,theo,logkey):
		super(TheoCache,self).__init__()
		self.theo = theo
		self.logkey = logkey
		self._init_cache()
	def _init_cache(self):
		self.cache = {}
		self.anyhead_cache = {}
	def _init_names(self):
		self._names = set()
	def reset_cache(self):
		logger(self.logkey).debug("reset_cache")
		self._init_cache()
		self._init_names()
	@property
	def names(self):
		if not hasattr(self,"_names"):
			self._init_names()
		return self._names
	def uncache(self, key, query, viz=lambda x:x.short_str(), eq=None, **kwargs):
		effects=0
		if not eq:
			eq = lambda x: x == query
		remove_name = False
		if isinstance(key,tuple):
			name,head = key
		elif key.find(".")<0:
			name,head = key,NO_THEO_VALUE
		else:
			name,head = key,None

		head_str = head.short_str() if head else str(head)
		logger(self.logkey).debug("uncaching (%s,%s)[%s] from \n%s",name,head_str,query,stackTrace(8))

		(name,head) in self

		def search_multivalue(cached):
			logger(self.logkey).debug("%s might be a cached value in %r",#\n%r",
									  query,[viz(x) for x in cached])#,cached)
			for ci in cached:
				if eq(ci):
					return ci
			# no matching ci found
			return False

		if not head:
			if name in self.anyhead_cache:
				to_remove = []
				for cached in self.anyhead_cache[name]:
					has = search_multivalue(cached)
					if has:
						cached.remove(has)
						effects |= 1
						logger(self.logkey).debug("removed %s from secondary value cache @%s",query,name)
						logger(self.logkey).debug("now %r",[viz(x) for x in cached])
					if has or len(cached)==0:
						if len(cached)==0:
							to_remove.append(cached)
						break # because no item is a member of more than one Multiple with different heads
				for cached in to_remove:
					self.anyhead_cache[name].remove(cached) # this has to be outside the anyhead_cache iter loop to work
					effects |= 2
					if cached.head or name.find(".")<0:
						headi = cached.head if cached.head else NO_THEO_VALUE
						if (name,headi) in self.cache:
							del self.cache[(name,headi)]
							effects |= 4
							logger(self.logkey).debug("removed %s from primary value cache @%s",query,(name,headi))
						# key = (name,cached.head if cached.head else NO_THEO_VALUE)
						# if key in self.cache:
						# 	del self.cache[key]
						# 	logger(self.logkey).debug("removed %s from primary value cache @%s",query,key)
				if len(self.anyhead_cache[name])==0:
					remove_name = True
					del self.anyhead_cache[name]
					effects |= 8
					logger(self.logkey).debug("removed %s from value cache index", key)
		elif (name,head) in self.cache:
			cached = self.cache[(name,head)]
			has = search_multivalue(cached)
			if has or len(cached)==0:
				if has:
					cached.remove(has)
					effects |= 0x10
					logger(self.logkey).debug("removed %s from primary value cache @%s",query,(name,head_str))
				if len(cached)==0:
					del self.cache[(name,head)]
					effects |= 0x20
					if name not in self.anyhead_cache:
						logger(self.logkey).debug("no %s in secondary value cache",name)
					else:
						self.anyhead_cache[name].remove(cached)
						effects |= 0x40
						logger(self.logkey).debug("removed %s from secondary value cache @%s",query,name)
						if len(self.anyhead_cache[name])==0:
							remove_name = True
							del self.anyhead_cache[name]
							effects |= 0x80
							logger(self.logkey).debug("removed %s from value cache index", (name,head_str))

		if remove_name:
			if name in self.names:
				self.names.remove(name)
				effects |= 0x100
				logger(self.logkey).debug("removed %s from names cache", name)
				assert (name,head) not in self, "removal of %s failed" % (name,head_str)

		return effects

	def __getitem__(self, key):
		if isinstance(key,tuple):
			name,head = key
		elif key.find(".")<0:
			name,head = key,NO_THEO_VALUE
		else:
			name,head = key,None
		headless=False

		head_str = head.short_str() if head else str(head)
		if head:
			if (name,head) in self.cache:
				return self.cache[(name,head)]
			logger(self.logkey).info("fq key (%s,%s) not found; trying headless",name,head_str)
			headless=True
		if name in self.anyhead_cache:
			if headless:logger(self.logkey).debug("fq key (%s,%s) headless fallback found",name,head_str)
			ret = self.anyhead_cache[name]
			if head and head != NO_THEO_VALUE:
				ret = [r for r in ret if r.head == head]
			if len(ret)>0:
				if len(ret)==1:
					if head:
						self.cache[(name,head)] = ret[0]
						logger(self.logkey).debug("caching from headless %s@%s = [%s]",name,head_str, ",".join(x.short_str() for x in ret[0]))
					return ret[0]
				logger(self.logkey).warning("%d>1 head available on headless query %s; returning all",len(ret),(name,head_str))
				return ret
		else: logger(self.logkey).debug("no headless record")
		## removed 10 july kmm: a fresh cache reset WILL contain root entities in self.names but WILL NOT contain (name,NO_THEO_VALUE) in self.cache
		# assert name not in self.names or not head, "Bad KB state: %s in names but not in cache"%name
		logger(self.logkey).debug("%s not found",name)
		return NO_THEO_VALUE
	def __setitem__(self, key, value):
		"""
		:param key: (path, head) for slots; (path, NO_THEO_VALUE) for entities
		:param value: TheoItem. if Singular, will be Multiple-ized; you'll want to set the head on slots.
		:return:
		"""
		ll = logger(self.logkey)
		if isinstance(key,tuple):
			name,head = key
		elif isinstance(value,Multiple):
			name,head = key,value.head
		elif key.find(".")<0:
			name,head = key,NO_THEO_VALUE
		else:
			name,head = key,None
		if head:
			assert (name,head) not in self.cache, "overwriting %s\n%r\n%r"%(
				(name,head),
				head.__dict__,
				self.cache[(name,head)].__dict__)
		head_str = head.short_str() if head else str(head)

		ll.debug("adding %s to names cache",name)
		self.names.add(name)
		if isinstance(value,Singular):
			value = self.theo.c.impl.Multiple(self.theo,value.name,[value])
			if head and head != NO_THEO_VALUE: value.head = head
		elif isinstance(value,Multiple):
			if head and value.head: assert value.head == head, f"mismatched head on kb.set ({name},{head}) = {value}"
		# if key in self.cache: logger('kb').warning("overwriting %s",key)
		self._cacheAdd(name, head, value, head_str, ll)
	def _cacheAdd(self, name, head, value, head_str, ll):
		if ll.isEnabledFor(logging.DEBUG):
			ll.debug("caching %s@%s = [%s]",name,head_str, ",".join(x.short_str() for x in value))
		if head:
			self.cache[(name,head)] = value
		if name not in self.anyhead_cache:
			self.anyhead_cache[name] = []
		elif head:
			for x in self.anyhead_cache[name]:
				if x == value:
					ll.warning("already present in anyhead cache: %s = %s",name,value.short_str())
					return
				assert head != x.head, "duplicate head on (%s,%s)\n%r\n%r\n\n%r\n%r"%(name,head_str,value,value.__dict__,x,x.__dict__)
		if ll.isEnabledFor(logging.DEBUG):
			ll.debug("caching %s@* = [%s]",name,",".join(x.short_str() for x in value))
		self.anyhead_cache[name].append(value)
	def __iter__(self):
		return self.cache.__iter__()
	def __contains__(self, key):
		if isinstance(key,tuple):
			name,head = key
		## removed 10 july kmm: a fresh cache reset WILL contain root entities in self.names but WILL NOT contain (name,NO_THEO_VALUE) in self.cache
		## so just don't even bother
		# elif key.find(".")<0:
		# 	name,head = key,NO_THEO_VALUE
		else:
			name,head = key,None
		logger(self.logkey).debug("__contains__ %s %s in names: %s; %s in cache: %s; (%s,NO_THEO_VALUE) in cache: %s; %s in anyhead_cache: %s",
								  hex(id(self)&0xffffffff),
								  name,name in self.names,
								  (name,head.short_str() if head else head), key in self.cache,
								  name,(name,NO_THEO_VALUE) in self.cache,
								  name,name in self.anyhead_cache)
		return name in self.names and ((name,head) in self.cache or not head)
	def keys(self):
		for n in self.names: yield n
	@classmethod
	def key_for(cls, multiple):
		return (multiple.name, NO_THEO_VALUE if multiple.name.find(".")<0 else multiple.head)

class ShelveTheoCache(TheoCache):
	def __init__(self,logkey,filename):
		self.filename = filename
		super(ShelveTheoCache,self).__init__(logkey)
	def _init_cache(self):
		self.cache = shelve.open(self.filename, writeback=True)
		self.anyhead_cache = shelve.open(f"{self.filename}.anyhead", writeback=True)


class TheoItem(object):
	"""The primary data type for a Theo KB.

	Each entry in the database has the following form:

	  (name value theoItem*)

	An entry at the base level of the hierarchy is an Entity. Entities usually have a NO_THEO_VALUE value.

	All other entries are Slots, aka Relations. These represent a relationship of type (name[-1]) between their
	parent node (which may be an Entity or another Slot) and the enclosed value.

	In this way a TheoItem represents some node in the graph as well as all relations for which it is the head."""

	@property
	def theo(self):
		"""Stores the Theo engine/KB for this item"""
		assert False, "Must implement 'theo' property in subclass %s" % type(self)

	@property
	def name(self):
		"""Stores the name for this item"""
		assert False, "Must implement 'name' property in subclass %s" % type(self)

	@property
	def value(self):
		"""Stores the value for this item"""
		assert False, "Must implement 'value' property in subclass %s" % type(self)

	@property
	def slots(self):
		"""Stores the list/set/iterable of slot names stored on this item"""
		assert False, "Must implement 'slots' property in subclass %s" % type(self)

	def slot_get(self, slot):
		assert False, "Must implement 'slot_get' method in subclass %s" % type(self)

	def slot_set(self, slot, ent):
		assert False, "Must implement 'slot_set' method in subclass %s" % type(self)

	def slot_delete(self, slot, ent=None, all=False):
		assert False, "Must implement 'slot_delete' method in subclass %s" % type(self)

	def address(self):
		assert False, "Must implement 'address' method in subclass %s" % type(self)


class Singular(TheoItem):
	def is_tmp(self):
		assert False, "Must implement 'is_tmp' method in subclass %s" % type(self)

	def raw_slot_get(self, slot):
		assert False, "Must implement 'raw_slot_get' method in subclass %s" % type(self)

	def raw_slot_set(self, slot, item):
		assert False, "Must implement 'raw_slot_set' method in subclass %s" % type(self)

	def has_slot(self, slot):
		assert False, "Must implement 'has_slot' method in subclass %s" % type(self)

	def _raw_slot_get(self, slot):
		"""Internal-only version"""
		return self.raw_slot_get(slot)
	def _raw_slot_set(self, slot, item):
		"""Internal-only version"""
		return self.raw_slot_set(slot, item)
	def _has_slot(self, slot):
		"""Internal-only version"""
		return self.has_slot(slot)

	def _add_to_kb(self):
		if not self.is_tmp():
			# cache root nodes when they are created; child (slot) nodes when they are set
			if self.name.find(".")<0:
				self.theo.KB[(self.name,NO_THEO_VALUE)] = self

	def slot_is(self, slot, value): # TODO: add arg exactly=False
		s = self.slot_get(slot)
		if s == NO_THEO_VALUE:
			return value==NO_THEO_VALUE
		if value==NO_THEO_VALUE: return False
		for v in s:
			if v.value == value: return True
		logger().debug("no value in %s matches %s",s,value)
		return False

	def slot_get(self, slot):
		ll = logger("cmd.slot_get")
		lchatty = logger("trace.slot_get")
		ll.debug("%s.%s", self.name, slot)
		if self._has_slot(slot):
			r = self._raw_slot_get(slot)
			ll.debug("%s.%s returning raw = %r", self.name, slot, r)
			return r
		lchatty.debug("  %s.%s apply_methods", self.name, slot)
		result = self.apply_methods(slot)
		if not result:
			v= self._raw_slot_get(slot)
			ll.debug("%s.%s apply_methods was false; returning raw value %r",self.name,slot,v)
			return v

		else:lchatty.debug("  %s.%s: unpacking apply_methods result %r",self.name,slot,result)
		result, method, probability, args, cachep = result
		fqname = f"{self.name}.{slot}"
		do_cache = False
		try:
			do_cache = cachep==True \
					   or (cachep=="ifValueKnown" and
						   all(v != NO_THEO_VALUE for v in (result.value if isinstance(result,TheoItem) else result)))
		except:
			ll.error('  %s.%s trouble unpacking cacheability from apply_methods %s results',self.name,slot,method)
			ll.error(result)
			raise
		# first draft at caching
		result = self.theo.c.impl.Multiple(
			self.theo,
			fqname,
			[
				self.theo.c.impl.Singular(self.theo, fqname, r, method=method, probability=probability, tmp=not do_cache) \
				for r in result
			],
			head=self)
		if do_cache:
			lchatty.info("caching %s.%s = %s",self.name,slot,str(result))
			result = self.slot_set(slot,result)
		ll.debug("%s.%s: returning %r",self.name,slot,result)
		return result
	def slot_set(self, attr, val, **kwargs):
		ll = logger("cmd.slot_set")
		lchatty = logger("trace.slot_set")
		ll.debug("%s.%s = %r " , self.name, attr, val)
		_attr=attr
		if isinstance(attr,TheoItem): attr = attr.name
		assert attr.find(".")<0,"slot_set with compound slot name %s"%attr
		fqname = f"{self.name}.{attr}"
		# if val == None:
		#     raise AttributeError(None)
		if isinstance(val, TheoItem) and val.name == fqname:
			# then this call is (probably) not sugared; leave val as specified
			pass
		else:
			val = self.theo.wrap_value(fqname, (val, ("generalizations",_attr)), pluralize=False, tmp=self.is_tmp(), **kwargs)
			lchatty.debug("wrapped to %s",val.short_str())
		if self._has_slot(attr):
			last = self._raw_slot_get(attr)
			assert isinstance(last,Multiple), f"Invalid result for {self.name}.{attr}: has_slot true but raw_slot_get is {last}"
			if len(last)==1 and last[0].value == NO_THEO_VALUE \
				and len(val.slots)==0:
					last[0].value = val.value
			else:
				rest = [val] if val not in last else []
				if isinstance(val, Multiple):
					rest = (x for x in val if x not in last)
				for x in rest:
					last.append(x)
		else:
			self._raw_slot_set(
				attr,
				val if isinstance(val, Multiple)
					else self.theo.c.impl.Multiple(self.theo, fqname, [val], head=self))
		if self.theo.c.preds.subtype == attr and val.value != NO_THEO_VALUE:
			vv = val.value
			lchatty.debug("checking generalizations of %s value" ,val)
			lchatty.debug("val.value: %s, %r", type(val.value), val.value)
			if not isinstance(vv, TheoConstant):
				if not isinstance(val, Multiple):
					vv = [vv]
				for v in vv:
					g = v.slot_get(self.theo.cpreds.supertype)
					lchatty.debug("  checking %s %r", type(g), g)
					if self not in g.value:
						v.slot_get(self.theo.c.preds.supertype).append(self)
					else:
						pass
		elif self.theo.c.preds.supertype == attr and val.value != NO_THEO_VALUE:
			vv = val.value
			lchatty.debug("checking specializations of %s value" ,val)
			lchatty.debug("val.value: %s %r", type(vv), vv)
			if not isinstance(vv, TheoConstant):
				if not isinstance(val, Multiple):
					vv = [vv]
				for v in vv:
					assert isinstance(v, Singular), "Unexpected nonsingular value for %r\n%r"%(val,v)
					lchatty.debug("  checking %s %r", type(v), v)
					# do not use internal _raw_slot_get because v is not self
					s = v.raw_slot_get(self.theo.c.preds.subtype, insert=True)
					specializations = [x.value for x in s if isinstance(x, TheoItem)]
					if lchatty.isEnabledFor(logging.DEBUG):
						lchatty.debug("   existing specializations:\n%s", "\n    ".join(
							(x.short_str() if isinstance(x, TheoItem) else "%r" % x) for x in specializations))
					if not any([self == x for x in specializations if isinstance(x, TheoItem)]
							   + [self in x for x in specializations if isinstance(x, list)]):
						lchatty.debug("   adding self")
						s.append(self.theo.wrap_value(s.name, self, False, tmp=self.is_tmp()))
					else:
						pass
		ret = self._raw_slot_get(attr)
		ll.debug("%s.%s returning %r",self.name,attr,ret)
		return ret

	def slot_delete(self, slot, ent=None, all=False):
		assert ent or all,"slot_delete must specify one of (ent, all)"
		if isinstance(slot,TheoItem):
			slot = slot.name
		if not self._has_slot(slot): return
		if all: return self._raw_slot_set(slot, None)
		current = self._raw_slot_get(slot)
		old_length=len(current)

		fqname = f"{self.name}.{slot}"
		if isinstance(ent, TheoItem) and ent.name == fqname:
			current.remove(ent)
			viz=lambda x:f"Entity {x.short_str()}"
		else:
			remove = [x for x in current if x.value==ent]
			for x in remove:
				current.remove(x)
			viz=lambda x:f"Value {x}"

		new_length = len(current)
		if new_length == old_length:
			raise TheoException("%s not found in (%s %s)" % (viz(ent), self.short_str(), slot))
		if new_length == 0:
			self._raw_slot_set(slot,None)

	def apply_methods(self, slot,slot_entity=None):
		ll = logger("cmd.apply_methods")
		lchatty = logger("trace.apply_methods")
		ll.debug("%s.%s", self.name, slot )
		probabilities = []
		args = []
		if slot_entity is None:
			se = self.theo.fetch(slot) # get the root definition for this slot
			lchatty.debug("%s %s apply_methods; %s = %r", self.name, slot,slot,se )
			if NO_THEO_VALUE == se:
				ll.debug("%s.%s returning n/a (slot entity is NO_VALUE)",self.name,slot)
				return
			assert isinstance(se[0],Singular), f"bad return from theo.fetch({slot}): {se}"
			se = se[0]
		else:
			assert isinstance(slot_entity,Singular), f"bad hint for {self.name}.{slot} slot_entity: {slot_entity}"
			se = slot_entity
		if self == se:
			ll.debug("%s.%s returning n/a (slot entity is me)",self.name,slot)
			return
		raw_methods = se.slot_get(self.theo.c.preds.methods)

		if NO_THEO_VALUE == raw_methods:
			ll.debug("%s.%s returning n/a (slot entity has no methods)",self.name,slot)
			return
		assert isinstance(raw_methods,TheoItem),"Bad return from %s.slot_get(%s): %r %r"%(se.name,self.theo.c.preds.methods,type(raw_methods),raw_methods)
		if all(vi == NO_THEO_VALUE for vi in raw_methods.value):
			ll.debug("%s.%s returning n/a (slot entity has no methods)",self.name,slot)
			return
		# logger().debug("%s %s apply_methods: %r", self.short_str(), slot, v )
		raw_methods = list(raw_methods.value)
		methods = flatten(raw_methods) if (raw_methods and isinstance(raw_methods[0],list)) else raw_methods
		lchatty.debug("%s %s apply_methods: %r", self.short_str(), slot, methods )
		for m in methods:
			assert self.theo.method(m), "Not a recognized method %s" % m
			result = getattr(self.theo, m)(self, slot)
			if m == 'prolog': result, probabilities, args = result
			if result is not None:
				raw_cachep = se.slot_get(self.theo.c.preds.cache)
				if NO_THEO_VALUE == raw_cachep:
					cachep = False
				else: cachep = all(v == "always" for v in raw_cachep.value) or first(lambda v: v != "always",raw_cachep.value)
				ll.debug("%s.%s returning success from %s",self.name,slot,m)
				return result, m, probabilities, args, cachep

		ll.debug("%s.%s returning n/a (no method succeeded)",self.name,slot)

	def __hash__(self):
		if not hasattr(self,"_hash"):
			self._hash = hash(self.name) + hash(repr(self.value) if isinstance(self.value, list) else self.value) + hash("".join(self.slots))
		return self._hash # this may interact poorly when objects are mutated

	def __str__(self):
		return self.str()

	def __repr__(self):
		return "TheoItemS:%s=%r+%r" % (self.name, self.value, self.slots)

	def lstr(self):
		return self.str(depth=-1, delim="\n")

	def str(self, depth=1, delim=""):
		if depth > 1 or depth < 0:
			slotclauses = ((x.str(depth - 1, delim + "\t") if isinstance(x, Singular)
							else (", ".join(xi.str(depth - 1, delim + "\t") for xi in x)) if isinstance(x, Multiple)
			else str(x)) # not internal _raw_slot_get to avoid heisenbugs related to logging
						   for x in [self.raw_slot_get(s) for s in self.slots])
		else:
			slotclauses = ((x.short_str() if isinstance(x, Singular)
							else (", ".join(xi.short_str() for xi in x)) if isinstance(x, Multiple)
			else str(x)) # not internal _raw_slot_get to avoid heisenbugs related to logging
						   for x in [self.raw_slot_get(s) for s in self.slots])
		return "%sTheoItemS(%s, %s, %s)" % (
			delim,
			self.name,
			self.value,
			", ".join(slotclauses))

	def short_str(self):
		if self.value != NO_THEO_VALUE:
			return "(%s=%s)" % (self.name,
								self.value.short_str()
								if isinstance(self.value, Singular) else \
									("[%d values]" % len(self.value) if len(self.value) > 1 else self.value[
										0].short_str())
									if isinstance(self.value, Multiple) else \
										self.value)
		else:
			return "(%s)" % self.name


class Multiple(TheoItem, list):
	def __init__(self, lst, head=None):
		for x in lst:
			assert isinstance(x, Singular), "I need a Singular here, not a %r" % type(x)
		assert head is None or isinstance(head,Singular), "I need a Singular here, not a %r" % type(head)
		self.head = head
		list.__init__(self, lst)

	@property
	def value(self):
		return (x.value for x in self)

	def slot_get(self, slot):
		logger("cmd.slot_get").debug("Multiple %s.%s",self.name,slot)
		ret = []
		has = False
		for x in self:
			try:
				ret.append(x.slot_get(slot))
				has = True
			except KeyError as AttributeError:
				ret.append(NO_THEO_VALUE)
		if has:
			logger("cmd.slot_get").debug("Multiple %s.%s returning %r",self.name,slot,ret)
			return ret
		raise AttributeError(slot)

	def _guard(self, other):
		if not isinstance(other, TheoItem): raise TheoException(
			"Can't add a %s here; you need a TheoItem[%s]" % (type(other), self.name))
		if not other.name == self.name: raise TheoException(
			"Can't add a %s here; you need a %s" % (other.name, self.name))
		return other

	def append(self, other):
		logger('Multiple').debug("slot_append %s %s", self.short_str(), other)
		other = self._guard(other)
		assert isinstance(other, Singular), "Can't append a %s here; you need a Singular" % type(other)
		list.append(self, other)

	def __str__(self):
		return "TheoItemP[%s]" % ", ".join(x.short_str() for x in self)

	def __repr__(self):
		return "TheoItemP:%s" % (super(Multiple,self).__repr__())

	def __eq__(self, other):
		if not isinstance(other,Multiple): return False
		if self.name != other.name: return False
		if self.head != other.head: return False
		if len(self) != len(other): return False
		for x,y in zip(self,other):
			if x != y: return False
		return True

	def short_str(self):
		return "(%s*%d)" % (self.name,len(self))


class NativeTheoItem(TheoItem):
	def __init__(self, theo, name):
		self.theo = theo
		self.name = name

	@property
	def theo(self): return self._theo

	@theo.setter
	def theo(self, t): self._theo = t

	@property
	def name(self): return self._name

	@name.setter
	def name(self, n): self._name = n


class NativeSingular(Singular, NativeTheoItem):
	def __init__(self, theo, name, value=NO_THEO_VALUE, tmp=True, *slot_tuples, **slot_kwargs):
		NativeTheoItem.__init__(self, theo, name)
		self._value = value
		self._slots = {}
		self.tmp = tmp
		self._add_to_kb()
		slots = self.theo.slots(self, tmp, *slot_tuples, **slot_kwargs)
		if slots:
			for k, v in slots:
				self.slot_set(k, v)

	@property
	def value(self):
		return self._value

	@value.setter
	def value(self, v):
		self._value = v

	@property
	def slots(self):
		return list(self._slots.keys())

	def has_slot(self, slot):
		local = slot in self._slots
		if local:
			# we never call has_slot in pytheo without a more-or-less immediate call to raw_slot_get,
			# so we lose nothing by explicitly checking our cached value is current
			check = self.raw_slot_get(slot)
			if check==NO_THEO_VALUE:
				logger('native').debug("[%s] has_slot %s: true locally + cache miss => removed spurious entry; returning False",
										  self.short_str(),slot)
				del self._slots[slot]
				return False
			logger('native').debug("[%s] has_slot %s: true locally (of %d slots)",self.short_str(),slot,len(self.slots))
			return True
		fqname = f"{self.name}.{slot}"
		assert not (fqname,self) in self.theo.KB, fqname
		return False

	def is_tmp(self): return self.tmp

	def raw_slot_get(self, slot, insert=False):
		key = (f"{self.name}.{slot}",self)
		if key in self.theo.KB:
			ret = self.theo.KB[key]
			return ret
		if not self.has_slot(slot):
			if insert:
				logger('native').debug("raw_slot_get: inserting empty value for %s on %s",
				slot, self.name)
				self._slots[slot] = self.theo.c.impl.Multiple(self.theo, f"{self.name}.{slot}", [], head=self)
			else:
				return NO_THEO_VALUE
		if not self.is_tmp():
			self.theo.KB[key] = self._slots[slot]
		return self._slots[slot]

	def raw_slot_set(self, slot, item):
		if item == None:
			if slot in self._slots:
				del self._slots[slot]
				self.theo.KB.uncache( (f"{self.name}.{slot}",self), None, eq=lambda x:True )
			return
		current_slot_value = self.raw_slot_get(slot,insert=False)
		if current_slot_value == NO_THEO_VALUE:
			assert item.head == self, f"{item.name} head not set before raw_slot_set"
			self._slots[slot] = item
			if not self.is_tmp(): self.theo.KB[(item.name,self)] = item
			return item
		items = item if isinstance(item,Multiple) else [item]
		remove = []
		for c in current_slot_value:
			if c not in items: remove.append(c)
		for r in remove: current_slot_value.remove(r)
		for i in items:
			if i not in current_slot_value:
				current_slot_value.append(i)
		return current_slot_value

class NativeMultiple(Multiple, NativeTheoItem):
	def __init__(self, theo, name, lst, head=None):
		NativeTheoItem.__init__(self, theo, name)
		Multiple.__init__(self, lst, head=head)


if __name__ == '__main__':
	# import logging
	# log = logging.getLogger("config")
	# log.addHandler(logging.StreamHandler())
	# log.setLevel(logging.DEBUG)

	theo = Theo()
	theo.load_kb()

	print("\nmake animal")
	animal = theo.Entity("animal")
	livesIn = theo.Slot("livesIn")
	babyName = theo.Slot("babyName")

	print("\nmake fox")
	fox = theo.Entity("fox", animal)
	fox.slot_set(livesIn, "burrow")
	fox.slot_set('livesIn', "zoo")
	fox.slot_set(babyName, "kit", source="wikipedia")

	print("\nKB listing")
	for address,head in theo.KB:
		if head != NO_THEO_VALUE: continue # root-level entities have NO_THEO_VALUE as their head
		print("\n".join(ki.lstr() if isinstance(ki,Singular) else str(ki) for ki in theo.KB[(address,head)]))

	print("\nget value")
	print(list(fox.slot_get('livesIn').value))

	print("\nfetch address")
	print(theo.fetch("fox", "babyName", "source"))

	print("\ndescendants of 'everything'")
	for k in theo.transitiveClosure(theo.KB['everything'][0], 'specializations'):
		print(k.short_str())

	print("\nancestors of 'fox'")
	for k in theo.transitiveClosure(fox, 'generalizations'):
		print(k.short_str())

	if theo.c.storage != "memory":
		theo.KB.close()
