# Adapted from Theo_matlab_9_24_09.tar:Theo/TheoKBs/theokb.m
#
# define the standard Theo knowledge base
# HISTORY
# 2/23/09 Tom added nrOfValues(probability), entities source, candidateValues
# 3/8/09 Tom changed defaultValue(nrOfValues) to 'any'
# 3/14/09 Tom removed 'inherit' from defaultValue of availableMethods
# 3/14/09 Tom added entities 'prologRules' and 'learnedPrologRules'
# 3/15/09 Tom added ability of 'prologRules' to infer rules from 'learnedPrologRules'
# 3/15/09 Tom changed defaultValue(whenToCache) to ifValueKnown
# 5/22/09 Tom added 'ifValueKnown' to legalValues(whenToCache)
#
# 2/4/2019 Katie ported to Python
#
# TODO: availableMethods should have nrOfValues=1, as it's a list, not a set!
import logging


def logger(suffix=None):
    name = __name__
    if suffix: name += "." + suffix
    return logging.getLogger(name)


def classic(theo,trace=False):
    if trace: trace.write("."); trace.flush()
    slot = theo.fetch('slot')[0]

    ## removed 15 jul 2019 by kmm: definingSlots is never used
    # slot.slot_set('definingSlots', ['whenToCache', 'legalValues', 'availableMethods', 'domain', 'range', 'nrOfValues'])

    ## 15 jul 2019 kmm: had to reorder things to set up inheritance correctly :(
    slotslot = theo.Entity('slotSlot', slot)
    theoSlot = theo.Entity('theoSlot', slot)
    comment = theo.Entity('comment', theoSlot)
    theoslotslot = theo.Entity('theoSlotSlot', slotslot)
    domain = theo.Entity('domain', theoslotslot)
    range = theo.Entity('range', theoslotslot)
    legalValues = theo.Entity('legalValues', 'theoSlotSlot')
    whenToCache = theo.Entity('whenToCache', 'theoSlotSlot')
    nrOfValues = theo.Entity('nrOfValues', 'theoSlotSlot')
    probability = theo.Entity('probability', 'theoSlotSlot')
    theo.Entity('justification', 'theoSlotSlot')
    availableMethods = theo.Entity('availableMethods', 'theoSlotSlot')


    theoSlot.slot_set(comment, 'below here are Theo system-defined slots. Put yours below slot instead.')

    if trace: trace.write("."); trace.flush()
    slotslot.slot_set(domain, slot)

    theoslotslot.slot_set(comment, 'below here are the Theo system-defined subslots.')

    if trace: trace.write("."); trace.flush()
    defaultValue = theo.Entity('defaultValue', theoslotslot)
    confidence = theo.Entity('confidence', theoslotslot)
    confidence.slot_set(legalValues, 'number')
    confidence.slot_set(comment, ['stores confidence in value of the ' + \
                        'associated slot'])

    if trace: trace.write("."); trace.flush()
    domain.slot_set(whenToCache, 'always')
    theoslotslot.slot_set(domain, slot)
    domain.slot_set(domain, slot)  # could infer this, but speeds things up
    domain.slot_set(defaultValue, 'everything')
    domain.slot_set(availableMethods, ['inherit', 'defaultValue'])

    if trace: trace.write("."); trace.flush()

    logger().debug("classic kb fetching generalizations")
    generalizations = theo.fetch("generalizations")[0]#Entity('generalizations', theoSlot)
    generalizations.slot_set(generalizations,theoSlot)
    generalizations.slot_set(nrOfValues, 'any')

    specializations = theo.fetch("specializations")[0]#Entity('specializations', theoSlot)
    specializations.slot_set("generalizations",theoSlot)
    specializations.slot_set(nrOfValues, 'any')

    if trace: trace.write("."); trace.flush()
    theo.Entity('subset', 'specializations')
    theo.fetch('subset')[0].slot_set(nrOfValues, 'any')
    theo.fetch('subset')[0].slot_set(range, slot)

    if trace: trace.write("."); trace.flush()
    theo.Entity('mutuallyExclusiveWith', 'theoSlot')
    theo.fetch('mutuallyExclusiveWith')[0].slot_set(nrOfValues, 'any')
    theo.fetch('mutuallyExclusiveWith')[0].slot_set(availableMethods, ['inherit'])

    if trace: trace.write("."); trace.flush()
    theo.Entity('inverse', 'theoSlotSlot')
    theo.fetch('inverse')[0].slot_set(domain, slot)
    theo.fetch('inverse')[0].slot_set(range, slot)

    if trace: trace.write("."); trace.flush()

    nrOfValues.slot_set(comment, 'indicates how many values the slot can have')
    nrOfValues.slot_set(legalValues, [1, 2, 'any'])
    nrOfValues.slot_set(defaultValue, 'any')
    nrOfValues.slot_set(availableMethods, ['defaultValue'])

    if trace: trace.write("."); trace.flush()
    probability.slot_set(comment, 'describes the probabilities associated with the slot values')
    probability.slot_set(availableMethods, [])
    probability.slot_set(nrOfValues, 'any')



    if trace: trace.write("."); trace.flush()
    theo.fetch('justification')[0].slot_set(comment, 'for now, this is just the name of the method used to infer its value.')
    theo.fetch('justification')[0].slot_set(availableMethods, [])


    # following is to implement slot value inference
    if trace:trace.write("."); trace.flush()
    availableMethods.slot_set(domain, slot)
    availableMethods.slot_set(whenToCache, 'always')
    #%% pv('whenToCache','availableMethods','always');  SHOULD BE 'IF TOP LEVEL', BUT NEED TO
    #IMPLEMENT THIS CACHING STRATEGY FIRST


    # 3/14/09 Tom removed 'inherit' from defaultValue of availableMethods
    availableMethods.slot_set(defaultValue, ['dropAllContext', 'defaultValue'])
    #pv('defaultValue','availableMethods',['dropAllContext','inherit',
    #                    'defaultValue'])
    availableMethods.slot_set(legalValues, ['dropAllContext', 'inherit', 'defaultValue', 'prolog'])

    slot.slot_set(availableMethods, ['dropAllContext', 'inherit', 'defaultValue'])
    availableMethods.slot_set(availableMethods, ['dropAllContext', 'inherit',
                        'defaultValue'])


    if trace: trace.write("."); trace.flush()
    generalizations.slot_set(availableMethods, ['defaultValue'])
    generalizations.slot_set(defaultValue, 'everything')
    ## specializations.defaultValue removed 5 jul 2019 by kmm.
    ##   #NO_THEO_VALUE serves the purpose better, no?
    # specializations.slot_set(defaultValue, None)
    # specializations.slot_set(availableMethods, ['defaultValue'])
    specializations.slot_set(availableMethods,[])
    availableMethods.slot_set(nrOfValues, 'any')

    if trace: trace.write("."); trace.flush()
    whenToCache.slot_set(defaultValue, 'ifValueKnown')
    whenToCache.slot_set(availableMethods, ['dropAllContext', 'inherit', 'defaultValue'])
    whenToCache.slot_set(whenToCache, 'always')
    whenToCache.slot_set(nrOfValues, 1)
    whenToCache.slot_set(legalValues, ['always', 'never', 'ifValueKnown'])
    whenToCache.slot_set(domain, slot)

    generalizations.slot_set(whenToCache, 'never')
    specializations.slot_set(whenToCache, 'never')
    comment.slot_set(whenToCache, 'never')

    if trace: trace.write("."); trace.flush()
    toget = theo.Entity('toget', 'theoSlotSlot')
    toget.slot_set(whenToCache, 'never')
    toget.slot_set(nrOfValues, 1)
    toget.slot_set(defaultValue, 'togetBare')
    toget.slot_set(toget, 'togetBare')

    if trace: trace.write("."); trace.flush()
    prologRules = theo.Entity('prologRules', 'theoSlotSlot')
    prologRules.slot_set(comment, 'stores horn clauses used to compute slots values for its domain element. Also copied from learnedPrologRules')
    prologRules.slot_set(availableMethods, ['prolog'])
    prologRules.slot_set(prologRules, [[[['prologRules' '?s' '?r'], ['learnedPrologRules' '?s' '?r']], 1.0]])
    prologRules.slot_set(defaultValue, [])
    prologRules.slot_set(whenToCache, 'never')
    prologRules.slot_set(nrOfValues, 'any')
    prologRules.slot_set(domain, slot)

    if trace: trace.write("."); trace.flush()
    learnedPrologRules = theo.Entity('learnedPrologRules', 'theoSlotSlot')
    learnedPrologRules.slot_set(comment, 'stores horn clauses learned by trainAndCommitFOLRs. These can be used by the prolog method to infer new slot values')
    learnedPrologRules.slot_set(availableMethods, [])
    learnedPrologRules.slot_set(defaultValue, [])
    learnedPrologRules.slot_set(whenToCache, 'never')
    learnedPrologRules.slot_set(nrOfValues, 'any')
    learnedPrologRules.slot_set(domain, slot)

    if trace: trace.write("."); trace.flush()
    theo.Entity('source', 'theoSlotSlot')
    theo.fetch('source')[0].slot_set(comment, 'subslot to store justifications for slot values')
    theo.fetch('source')[0].slot_set(whenToCache, 'never')
    theo.fetch('source')[0].slot_set(nrOfValues, 'any')

    if trace: trace.write("."); trace.flush()
    theo.Entity('candidateValues', 'theoSlotSlot')
    theo.fetch('candidateValues')[0].slot_set(comment, 'subslot to store alternative candidate values for slots whose nrOfValues=1')
    theo.fetch('candidateValues')[0].slot_set(whenToCache, 'never')
    theo.fetch('candidateValues')[0].slot_set(nrOfValues, 'any')
