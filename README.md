# PyTheo

This is a Python implementation of Theo[1], a knowledge base framework for self-improving systems.

System requirements:

- Python 3.6+
- config (pip install config)

Demo: see also __main__ of theo.py

```python
	theo = Theo()
	theo.load_kb()

	print("\nmake animal")
	animal = theo.Entity("animal")
	livesIn = theo.Slot("livesIn")
	babyName = theo.Slot("babyName")

	print("\nmake fox")
	fox = theo.Entity("fox", animal)
	fox.slot_set(livesIn, "burrow")
	fox.slot_set('livesIn', "zoo")
	fox.slot_set(babyName, "kit", source="wikipedia")

	print("\nKB listing")
	for address,head in theo.KB:
	    # root-level entities have NO_THEO_VALUE as their head
		if head != NO_THEO_VALUE: 
		    continue 
		print("\n".join(ki.lstr() for ki in theo.KB[(address,head)]))

	print("\nget value")
	print(list(fox.slot_get('livesIn').value))

	print("\nfetch address")
	print(theo.fetch("fox", "babyName", "source"))

	print("\ndescendants of 'everything'")
	for k in theo.transitiveClosure(theo.KB['everything'][0], 'specializations'):
		print(k.short_str())

	print("\nancestors of 'fox'")
	for k in theo.transitiveClosure(fox, 'generalizations'):
		print(k.short_str())

	if theo.c.storage != "memory":
		theo.KB.close()
```

Expected output:

    $ python pytheo/theo.py
    
        make animal
    
        make fox
    
        KB listing
    
        TheoItemS(everything, #NO_THEO_VALUE,   
            TheoItemS(everything.specializations, TheoItemS(slot, #NO_THEO_VALUE, (slot.generalizations=(everything)), (slot.specializations=(specializations)), (slot.specializations=(generalizations)), (slot.specializations=(livesIn)), (slot.specializations=(babyName))), ),
            TheoItemS(everything.specializations, TheoItemS(animal, #NO_THEO_VALUE, (animal.generalizations=(everything)), (animal.specializations=(fox))), ))
        
        TheoItemS(slot, #NO_THEO_VALUE, 
            TheoItemS(slot.generalizations, TheoItemS(everything, #NO_THEO_VALUE, (everything.specializations=(slot)), (everything.specializations=(animal))), ), 
            TheoItemS(slot.specializations, TheoItemS(specializations, #NO_THEO_VALUE, (specializations.generalizations=(slot))), ), 
            TheoItemS(slot.specializations, TheoItemS(generalizations, #NO_THEO_VALUE, (generalizations.generalizations=(slot))), ), 
            TheoItemS(slot.specializations, TheoItemS(livesIn, #NO_THEO_VALUE, (livesIn.generalizations=(slot)), (livesIn.specializations=(fox.livesIn=burrow)), (livesIn.specializations=(fox.livesIn=zoo))), ), 
            TheoItemS(slot.specializations, TheoItemS(babyName, #NO_THEO_VALUE, (babyName.generalizations=(slot)), (babyName.specializations=(fox.babyName=kit))), ))
        
        TheoItemS(specializations, #NO_THEO_VALUE, 
            TheoItemS(specializations.generalizations, TheoItemS(slot, #NO_THEO_VALUE, (slot.generalizations=(everything)), (slot.specializations=(specializations)), (slot.specializations=(generalizations)), (slot.specializations=(livesIn)), (slot.specializations=(babyName))), ))
        
        TheoItemS(generalizations, #NO_THEO_VALUE, 
            TheoItemS(generalizations.generalizations, TheoItemS(slot, #NO_THEO_VALUE, (slot.generalizations=(everything)), (slot.specializations=(specializations)), (slot.specializations=(generalizations)), (slot.specializations=(livesIn)), (slot.specializations=(babyName))), ))
        
        TheoItemS(animal, #NO_THEO_VALUE, 
            TheoItemS(animal.generalizations, TheoItemS(everything, #NO_THEO_VALUE, (everything.specializations=(slot)), (everything.specializations=(animal))), ), 
            TheoItemS(animal.specializations, TheoItemS(fox, #NO_THEO_VALUE, (fox.generalizations=(animal)), (fox.livesIn=burrow), (fox.livesIn=zoo), (fox.babyName=kit)), ))
        
        TheoItemS(livesIn, #NO_THEO_VALUE, 
            TheoItemS(livesIn.generalizations, TheoItemS(slot, #NO_THEO_VALUE, (slot.generalizations=(everything)), (slot.specializations=(specializations)), (slot.specializations=(generalizations)), (slot.specializations=(livesIn)), (slot.specializations=(babyName))), ), 
            TheoItemS(livesIn.specializations, TheoItemS(fox.livesIn, burrow, (fox.livesIn.generalizations=(livesIn))), ), 
            TheoItemS(livesIn.specializations, TheoItemS(fox.livesIn, zoo, (fox.livesIn.generalizations=(livesIn))), ))
        
        TheoItemS(babyName, #NO_THEO_VALUE, 
            TheoItemS(babyName.generalizations, TheoItemS(slot, #NO_THEO_VALUE, (slot.generalizations=(everything)), (slot.specializations=(specializations)), (slot.specializations=(generalizations)), (slot.specializations=(livesIn)), (slot.specializations=(babyName))), ), 
            TheoItemS(babyName.specializations, TheoItemS(fox.babyName, kit, (fox.babyName.generalizations=(babyName)), (fox.babyName.source=wikipedia)), ))
        
        TheoItemS(fox, #NO_THEO_VALUE, 
            TheoItemS(fox.generalizations, TheoItemS(animal, #NO_THEO_VALUE, (animal.generalizations=(everything)), (animal.specializations=(fox))), ), 
            TheoItemS(fox.livesIn, burrow, 
                    TheoItemS(fox.livesIn.generalizations, TheoItemS(livesIn, #NO_THEO_VALUE, (livesIn.generalizations=(slot)), (livesIn.specializations=(fox.livesIn=burrow)), (livesIn.specializations=(fox.livesIn=zoo))), )), 
            TheoItemS(fox.livesIn, zoo, 
                    TheoItemS(fox.livesIn.generalizations, TheoItemS(livesIn, #NO_THEO_VALUE, (livesIn.generalizations=(slot)), (livesIn.specializations=(fox.livesIn=burrow)), (livesIn.specializations=(fox.livesIn=zoo))), )), 
            TheoItemS(fox.babyName, kit, 
                    TheoItemS(fox.babyName.generalizations, TheoItemS(babyName, #NO_THEO_VALUE, (babyName.generalizations=(slot)), (babyName.specializations=(fox.babyName=kit))), ), 
                    TheoItemS(fox.babyName.source, wikipedia, )))
        
        get value
        ['burrow', 'zoo']
        
        fetch address
        TheoItemP[(fox.babyName.source=wikipedia)]
        
        descendants of 'everything'
        (everything)
        (slot)
        (specializations)
        (generalizations)
        (livesIn)
        (fox.livesIn=burrow)
        (fox.livesIn=zoo)
        (babyName)
        (fox.babyName=kit)
        (animal)
        (fox)
        
        ancestors of 'fox'
        (fox)
        (animal)
        (everything)




[1] "Theo: A Framework for Self-Improving Systems", Tom M. Mitchell, John Allen, Prasad Chalasani, John Cheng, Oren Etzioni, Marc N. Ringuette, Jeffrey C. Schlimmer, in "Architectures for Intelligence", K. Vanlehn (ed.), Lawrence Erlbaum Associates, New Jersey, 1991, pp. 323-356.