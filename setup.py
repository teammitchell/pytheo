import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pytheo-teammitchell",
    version="0.1.0",
    author="K Mazaitis for Carnegie Mellon University",
    author_email="krivard@cs.cmu.edu",
    description="A python implementation of Theo",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/teammitchell/pytheo",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
